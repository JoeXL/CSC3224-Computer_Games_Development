#pragma once
#include "XLEvent.h"
#include <queue>
#include <memory>

class XLEventQueue
{
public:
	XLEventQueue();
	~XLEventQueue();

	void push(XLEvent* event) {
		eventQueue.push(event);
	}

	bool empty() { return eventQueue.empty(); }


	int size() { return eventQueue.size(); }


	//Get oldest element
	XLEvent* front() { return eventQueue.front(); }

	//Get newest element
	XLEvent* back() { return eventQueue.back(); }

	//Remove oldest element, note: does not call destructor
	void popEvent() {
		return eventQueue.pop(); }

private:
	std::queue<XLEvent*> eventQueue;
};

