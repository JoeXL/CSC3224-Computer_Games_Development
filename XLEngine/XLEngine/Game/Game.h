#pragma once

#include "../XLEngine/XLEngine.h"
#include <Windows.h>
#include <iostream>
#include <vector>
#include "Enemy.h"
#include "Player.h"
#include "Bullet.h"

enum KEYBINDS {
	CHAR_FORWARD, CHAR_BACK, CHAR_LEFT, CHAR_RIGHT, CHAR_SHOOT
};

class Game
{
public:
	Game();
	~Game();

	/* Contains the main game loop */
	void run();
	/* If the player is alive and the enemies can see it, move towards it */
	void moveEnemies();

	void addEnemy(float x, float z);
	void addWall(float x, float z);
	void addFloor(float x, float z);
	void addPlayer(float x, float z);
	void addExit(float x, float z);
	void addExitWall(float x, float z);
	void addBullet(float x, float z, Vector3 dir);

	/* Spawn particles at the position of the enemy which move away from the bullet position*/
	void spawnDeathParticles(Enemy* enemy, Bullet* bullet);

	void removeEnemy(string id);
	void removeBullet(string id);

	/* Perform ray casts from each enemy to the player */
	void generateEnemyVisionRays();

	void loadLevel(int level);
	void unloadLevel();

	/* Helper function for checking collision data */
	bool checkCollisionForTags(XLCollisionEvent* e, string s1, string s2);

	/* Set game variables such as player health */
	void setup();

private:

	float screenWidth = 0;
	float screenHeight = 0;
	float playerRotation = 0;

	vector<Enemy*> enemies;
	vector<Entity*> walls;
	vector<Entity*> fakeWalls;
	vector<Entity*> floors;
	vector<Entity*> deathParticles;
	vector<Bullet*> bullets;
	vector<Entity*> exitWalls;
	Player player;
	float lastFrameStartTime = 0;
	float lastFrameEndTime = 0;

	float msec = 0;
	string bgMusicFile;

	bool doLoadLevel = false;

	bool reachedExit = false;

	bool levelCleared = false;

	bool exitGame = false;
	bool lastLevel = false;

	int currentLevel = 0;

	XLEngine engine;

	float playerHurtSoundTimeMax = 100;
	float playerHurtSoundTimer = 0;

	float playerShootTimeMax = 150;
	float playerShootTimer = 0;

	int deathParticleCount = 5;
	int playerHealth = 10;
	int enemyHealth = 10;
	int bulletDamage = 10;
	int enemyDamage = 10;

	bool gamepadActive = false;

	/* Helper function for player rotation to mouse */
	float angleBetweenTwoPoints(Vector2 coord1, Vector2 coord2) {
		if ((coord1.x == coord2.x) && (coord1.y == coord2.y))
			return 0;
		float angle = atan((coord1.x - coord2.x) / (coord1.y - coord2.y)) * (180.0f / PI);
		if (coord1.y < coord2.y)
			angle = -angle;
		else
			angle = -angle + 180;

		return angle;
	}
};

