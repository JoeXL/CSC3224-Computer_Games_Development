#pragma once
#include "../XLEngine/XLEngine.h"

enum CharacterType { PLAYER, ENEMY };

class Character
{
public:
	Character() {}
	~Character();
	CharacterType getType() { return type; }

	Entity* getEntity() { return entity; }
	void setEntity(Entity* e) { entity = e; }

	float getHP() { return hp; }
	void setHP(float health) { hp = health; }

	string getId() { return id; }
	void setId(string s) { id = s; }

protected:
	Entity* entity;
	float hp = 0;
	CharacterType type;
	string id;
};

