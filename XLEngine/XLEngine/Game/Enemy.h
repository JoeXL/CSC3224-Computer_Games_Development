#pragma once
#include "Character.h"
#include "Bullet.h"

class Enemy : public Character
{
public:
	Enemy() { type = ENEMY; }
	~Enemy();

	Bullet* getShotBy() { return shotBy; }
	void setShotBy(Bullet* bullet) { shotBy = bullet; }

	bool canSeePlayer() { return bCanSeePlayer; }
	void setCanSeePlayer(bool b) { bCanSeePlayer = b; }

private:
	Bullet* shotBy;
	bool bCanSeePlayer = false;
};

