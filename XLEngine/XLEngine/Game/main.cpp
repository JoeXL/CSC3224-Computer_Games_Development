// Game.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "../XLEngine/XLEngine.h"
#include <Windows.h>
#include <iostream>
#include <vector>
#include "Game.h"

int main()
{

	Game game;
	game.run();

	return 0;
}