#pragma once
#include "../XLEngine/XLEngine.h"

class Bullet
{
public:
	Bullet();
	~Bullet();

	Entity* getEntity() { return entity; }
	void setEntity(Entity* e) { entity = e; }

	int getDamage() { return damage; }
	void setDamage(int dam) { damage = dam; }

	Vector3 getDirection() { return direction; }
	void setDirection(Vector3 dir) { direction = dir; }

	string getId() { return id; }
	void setId(string s) { id = s; }

	bool getCollided() { return collided; }
	void setCollided(bool c) { collided = c; }

protected:
	Entity* entity;
	int damage;
	string id;
	Vector3 direction;
	bool collided = false;
};

