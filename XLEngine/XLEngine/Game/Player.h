#pragma once
#include "Character.h"
class Player : public Character
{
public:
	Player() { type = PLAYER; }
	~Player();

	void setAlive(bool b) { alive = b; }
	bool isAlive() { return alive; }

private:
	bool alive = false;
};

