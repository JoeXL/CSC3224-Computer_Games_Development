// Game.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include "Game.h"
#include "time.h"

Game::Game()
{
	/* Set window settings from startup file */
	FileReader startUpInfo = engine.getFileReader();
	startUpInfo.loadFile("startup.txt");
	FileData windowTitle = startUpInfo.getDataOfType("Window Title");
	FileData resWidth = startUpInfo.getDataOfType("Resolution Width");
	FileData resHeight = startUpInfo.getDataOfType("Resolution Height");
	FileData full_screen = startUpInfo.getDataOfType("Fullscreen");

	string::size_type sz;

	string title = windowTitle.getData();
	int width = stoi(resWidth.getData(), &sz);
	screenWidth = width;
	int height = stoi(resHeight.getData(), &sz);;
	screenHeight = height;
	bool fullscreen = false;
	if (full_screen.getData() == "true")
		fullscreen = true;

	srand(time(NULL));

	engine.initialize(title, width, height, fullscreen);
}


Game::~Game()
{
	unloadLevel();
}

void ::Game::run() {

	setup();

	/* Load meshes */
	engine.loadMesh("cube", "cube2.obj");
	engine.loadMesh("sphere", "sphere.obj");

	/* Load textures */
	engine.loadTexture("player", "player.jpg");
	engine.loadTexture("floor", "floor.jpg");
	engine.loadTexture("wall", "wall.jpg");
	engine.loadTexture("enemy", "enemy.jpg");
	engine.loadTexture("exit", "exit.jpg");
	engine.loadTexture("enemyDeath", "enemy_death.jpg");

	/* Setup keybinds */
	engine.addInterfaceEvent(new XLKeybindEvent(KEYBOARD, KEYBOARD_W, CHAR_FORWARD));
	engine.addInterfaceEvent(new XLKeybindEvent(KEYBOARD, KEYBOARD_S, CHAR_BACK));
	engine.addInterfaceEvent(new XLKeybindEvent(KEYBOARD, KEYBOARD_A, CHAR_LEFT));
	engine.addInterfaceEvent(new XLKeybindEvent(KEYBOARD, KEYBOARD_D, CHAR_RIGHT));
	engine.addInterfaceEvent(new XLKeybindEvent(MOUSE, MOUSE_LEFT, CHAR_SHOOT));

	engine.addInterfaceEvent(new XLKeybindEvent(GAMEPAD, XBOX_RIGHT_SHOULDER, CHAR_SHOOT));

	/* Load first level */
	loadLevel(currentLevel);

	/* Main game loop */
	while (engine.getWindow()->UpdateWindow() && !Window::GetKeyboard()->KeyDown(KEYBOARD_ESCAPE) && !exitGame) {
		/* If player is dead or exit reached, load a level */
		if (doLoadLevel) {
			loadLevel(currentLevel);
			doLoadLevel = false;
		}

		/* If the player is alive, set the camera position */
		if (player.isAlive()) {
			Vector3 playerPos = player.getEntity()->getTransform().GetPositionVector();
			playerPos.y = 2;
			engine.setCameraPosition(playerPos + Vector3(10, 15, 10));
			engine.setCameraPitch(-45);
			engine.setCameraYaw(45);
		}

		/* Update the engine */
		engine.update();

		/* Calculate milli since last frame */
		msec = engine.getWindow()->GetTimer()->GetMS() - lastFrameStartTime;
		lastFrameStartTime = engine.getWindow()->GetTimer()->GetMS();

		/* Update timers */
		playerHurtSoundTimer -= msec;
		playerShootTimer -= msec;

		/* Process incoming physics events */
		XLEventQueue* physicsEvents = engine.getPhysicsEvents();
		while (!physicsEvents->empty()) {
			XLPhysicsEvent* physicsEvent = (XLPhysicsEvent*)physicsEvents->front();
			switch (physicsEvent->getType()) {
			case COLLISION: { // Collision events
				XLCollisionEvent* e = (XLCollisionEvent*)physicsEvent;
				/* On player-exit collision, set reached exit */
				if(checkCollisionForTags(e, "player", "exit")) {
					if (levelCleared) {
						reachedExit = true;
					}
				}
				/* On player-enemy collision, deal damage to player */
				else if (checkCollisionForTags(e, "player", "enemy")) {
					player.setHP(player.getHP() - ((msec / 1000) * enemyDamage));
					if (playerHurtSoundTimer <= 0) {
						XL2DPlaySoundEvent* soundEvent = new XL2DPlaySoundEvent("playerHit.wav");
						engine.addSoundEvent(soundEvent);
						playerHurtSoundTimer = playerHurtSoundTimeMax;
					}
				}
				/* On wall-bullet collision, destory the bullet */
				else if (checkCollisionForTags(e, "wall", "bullet")) {
					if (e->idA.substr(0, 6) == "bullet") {
						for (int i = 0; i < bullets.size(); ++i) {
							if (bullets.at(i)->getId() == e->idA) {
								bullets.at(i)->setCollided(true);
							}
						}
					}
					else {
						for (int i = 0; i < bullets.size(); ++i) {
							if (bullets.at(i)->getId() == e->idB) {
								bullets.at(i)->setCollided(true);
							}
						}
					}
				}
				/* On bullet-enemy collision, destroy the bullet and deal damage to the enemy */
				else if (checkCollisionForTags(e, "enemy", "bullet")) {
					Bullet* bullet;
					Enemy* enemy;
					if (e->idA.substr(0, 6) == "bullet") {
						for (int i = 0; i < bullets.size(); ++i) {
							if (bullets.at(i)->getId() == e->idA) {
								bullet = bullets.at(i);
							}
						}
						for (int i = 0; i < enemies.size(); ++i) {
							if (enemies.at(i)->getId() == e->idB) {
								enemy = enemies.at(i);
							}
						}
					}
					else {
						for (int i = 0; i < bullets.size(); ++i) {
							if (bullets.at(i)->getId() == e->idB) {
								bullet = bullets.at(i);
							}
						}
						for (int i = 0; i < enemies.size(); ++i) {
							if (enemies.at(i)->getId() == e->idA) {
								enemy = enemies.at(i);
							}
						}
					}
					enemy->setHP(enemy->getHP() - bullet->getDamage());
					enemy->setShotBy(bullet);
					bullet->setCollided(true);
					XL2DPlaySoundEvent* soundEvent = new XL2DPlaySoundEvent("enemyHit.wav");
					engine.addSoundEvent(soundEvent);
				}
				delete e;
				break;
			}
			case RETURN_RAY_ALL_HITS_INFO: { // Ray trace events (all these are enemy line-of-sight checks)
				XLReturnRayAllHitsEvent* e = (XLReturnRayAllHitsEvent*)physicsEvent;
				bool hitsWall = false;
				for (int i = 0; i < e->hitEntityIds.size(); ++i) { // If there is a wall between the enemy and player, enemy can see the player
					if (e->hitEntityIds.at(i).substr(0, 4) == "wall")
						hitsWall = true;
				}
				if(!hitsWall) {
					for (int i = 0; i < enemies.size(); ++i) {
						if (e->rayId == enemies.at(i)->getId()) {
							enemies.at(i)->setCanSeePlayer(true);
						}
					}
				}
				else {
					for (int i = 0; i < enemies.size(); ++i) {
						if (e->rayId == enemies.at(i)->getId()) {
							enemies.at(i)->setCanSeePlayer(false);
						}
					}
				}
				delete e;
				break;
			}
			}
			physicsEvents->popEvent();
		}

		if (!levelCleared) { // If the level has not been cleared but the enemy count is 0, lower the exit walls
			if (enemies.size() == 0) {
				for (int i = 0; i < exitWalls.size(); ++i) {
					XLSetPositionEvent* e = new XLSetPositionEvent;
					e->id = exitWalls.at(i)->getId();
					Vector3 newPos = exitWalls.at(i)->getPosition();
					newPos.y -= 2;
					e->position = newPos;
					engine.addPhysicsEvent(e);
				}
				levelCleared = true;
			}
		}

		/* Process incoming interface events */
		XLEventQueue* interfaceEvents = engine.getInterfaceEvents();
		while (!interfaceEvents->empty()) {
			XLInterfaceEvent* interEvent = (XLInterfaceEvent*)interfaceEvents->front();
			switch (interEvent->getType()) {
			case BUTTON_PRESS: {
				XLInterfaceButtonEvent* buttonEvent = (XLInterfaceButtonEvent*)interEvent;
				if (buttonEvent->controller == GAMEPAD) {
					gamepadActive = true;
				}
				else {
					gamepadActive = false;
				}
				switch (buttonEvent->function) {
				case CHAR_FORWARD: {
					XLForceEvent* forceEvent = new XLForceEvent;
					forceEvent->id = "player";
					forceEvent->force = Vector3(-10, 0, -10);
					engine.addPhysicsEvent(forceEvent);
					delete buttonEvent;
					break;
				}
				case CHAR_BACK: {
					XLForceEvent* forceEvent = new XLForceEvent;
					forceEvent->id = "player";
					forceEvent->force = Vector3(10, 0, 10);
					engine.addPhysicsEvent(forceEvent);
					delete buttonEvent;
					break;
				}
				case CHAR_LEFT: {
					XLForceEvent* forceEvent = new XLForceEvent;
					forceEvent->id = "player";
					forceEvent->force = Vector3(-10, 0, 10);
					engine.addPhysicsEvent(forceEvent);
					delete buttonEvent;
					break;
				}
				case CHAR_RIGHT: {
					XLForceEvent* forceEvent = new XLForceEvent;
					forceEvent->id = "player";
					forceEvent->force = Vector3(10, 0, -10);
					engine.addPhysicsEvent(forceEvent);
					delete buttonEvent;
					break;
				}
				case CHAR_SHOOT: { // Create a bullet infront of the player with a velocity in that direction
					if (playerShootTimer <= 0) {
						if (!buttonEvent->held) {
							if (player.isAlive()) {
								playerShootTimer = playerShootTimeMax;
								Matrix4 rot = Matrix4::Rotation(player.getEntity()->getRotationAngle(), player.getEntity()->getRotationAxis());
								Vector3 direction = rot * Vector3(0, 0, 1);
								direction.Normalise();

								Vector3 pos = player.getEntity()->getPosition() + (direction * 0.55);
								pos.y = 0;

								addBullet(pos.x, pos.z, direction);

								XL2DPlaySoundEvent* soundEvent = new XL2DPlaySoundEvent("shoot02.wav");
								engine.addSoundEvent(soundEvent);

							}
						}
					}
				}
				}
				break;

			}
			case MOUSE_DATA: { // Calculate rotation for player to face the camera
				XLMouseDataEvent* mouseData = (XLMouseDataEvent*)interEvent;
				if (!gamepadActive) {
					Vector2 screenSize = engine.getScreenSize();
					playerRotation = -(angleBetweenTwoPoints(Vector2(screenSize.x / 2.0f, (screenSize.y / 2.0f) + (screenSize.y / 60.0f)), mouseData->absolutePosition) - 45);
				}
				delete mouseData;
				break;
			}
			case GAMEPAD_DATA: {
				XLGamepadDataEvent* padData = (XLGamepadDataEvent*)interEvent;
				if (gamepadActive) {
					XLForceEvent* forceEvent = new XLForceEvent;
					forceEvent->id = "player";
					forceEvent->force = Matrix4::Rotation(45, Vector3(0, 1, 0)) * Vector3(padData->leftThumbX * 10, 0, -padData->leftThumbY * 10);
					engine.addPhysicsEvent(forceEvent);
					playerRotation = (angleBetweenTwoPoints(Vector2(0, 0), Vector2(padData->rightThumbX, padData->rightThumbY)) + 45) + 180;
				}
				delete padData;
				break;
			}
			}
			interfaceEvents->popEvent();
		}

		/* If the player is dead, load a level */
		if (!player.isAlive()) {
			doLoadLevel = true;
		}
		else if (reachedExit) { // If the exit has been reached, the next level load to be the next level and stop the background music
			unloadLevel();
			++currentLevel;
			doLoadLevel;
			reachedExit = false;

			XLUnloadSoundEvent* unSoundEvent = new XLUnloadSoundEvent(bgMusicFile);
			engine.addSoundEvent(unSoundEvent);
		}

		/* If the player is alive, set its rotate to be towards the mouse */
		if (player.isAlive()) {
			XLSetRotationEvent* rot = new XLSetRotationEvent;
			float turnSpeed = 20;
			float angle = engine.getEntity("player")->getRotationAngle();
			Vector3 axis = engine.getEntity("player")->getRotationAxis();
			axis.Normalise();
			Vector3 foo = Vector3(0, 1, 0);
			if (angle > 240 && axis.y < 0) { foo = Vector3(0, -1, 0); angle -= (120 + turnSpeed); }
			else if (angle < 240 && axis.y < 0) { foo = Vector3(0, -1, 0); angle -= turnSpeed; }
			else { angle += turnSpeed; }
			rot->id = "player";
			rot->axis = Vector3(0, 1, 0);
			rot->angle = playerRotation;
			engine.addPhysicsEvent(rot);
		}

		/* Remove dead enemies */
		for (int i = 0; i < enemies.size(); ++i) {
			if (enemies.at(i)->getHP() <= 0) {
				XL2DPlaySoundEvent* soundEvent = new XL2DPlaySoundEvent("enemyDeath01.wav");
				engine.addSoundEvent(soundEvent);
				removeEnemy(enemies.at(i)->getId());
			}
		}
		/* Remove collided bullets */
		for (int i = 0; i < bullets.size(); ++i) {
			if (bullets.at(i)->getCollided()) {
				removeBullet(bullets.at(i)->getId());
			}
		}

		moveEnemies();
		generateEnemyVisionRays();

		/* If the player is dead, set variables and unload the level */
		if (player.getHP() <= 0) {
			if (player.isAlive()) {
				XL2DPlaySoundEvent* soundEvent = new XL2DPlaySoundEvent("playerDeath01.wav");
				engine.addSoundEvent(soundEvent);

				XLUnloadSoundEvent* unSoundEvent = new XLUnloadSoundEvent(bgMusicFile);
				engine.addSoundEvent(unSoundEvent);
			}
			player.setEntity(nullptr);
			engine.removeEntity("player");
			player.setAlive(false);
			unloadLevel();
		}
	}
}

void Game::moveEnemies() {
	if (player.isAlive()) {
		for (int i = 0; i < enemies.size(); ++i) {
			if (enemies.at(i)->canSeePlayer()) {
				XLForceEvent* e = new XLForceEvent;
				e->id = enemies.at(i)->getId();
				Vector3 force = engine.getEntity("player")->getTransform().GetPositionVector() - enemies.at(i)->getEntity()->getTransform().GetPositionVector();
				force.y = 0;
				force.Normalise();
				e->force = force * 1;
				e->forceLocation = Vector3(0, 0, 0);
				engine.addPhysicsEvent(e);
			}
		}
	}
}

void Game::addEnemy(float x, float z) {
	string id = "enemy" + to_string(enemies.size());
	if (engine.getEntity(id)) {
		for (int j = 0; j < enemies.size(); ++j) {
			if (!engine.getEntity(string("enemy" + to_string(j)))) {
				id = "enemy" + to_string(j);
			}
		}
	}
	engine.addEntity(id);
	Enemy* enemy = new Enemy;
	enemy->setEntity(engine.getEntity(id));
	enemy->setId(id);
	enemy->setHP(enemyHealth);
	enemy->setCanSeePlayer(false);
	enemies.push_back(enemy);
	engine.setEntityMesh(id, "cube");
	engine.setEntityTexture(id, "enemy");
	engine.setEntityRenderScale(id, Vector3(1.0f, 1.0f, 1.0f));
	XLNewPhysicsEntityEvent* e = new XLNewPhysicsEntityEvent;
	e->id = id;
	e->shape = PHYSICS_BOX;
	e->position = Vector3(x, 1.5f, z);
	e->mass = 0.1;
	e->scale = Vector3(0.5f, 0.5f, 0.5f);
	e->gravity = true;
	engine.addPhysicsEvent(e);

	XLRotationLimiterEvent* rotLimitEvent = new XLRotationLimiterEvent;
	rotLimitEvent->id = id;
	rotLimitEvent->axis = Vector3(0, 1, 0);
	engine.addPhysicsEvent(rotLimitEvent);

	XLMovementLimiterEvent* movLimitEvent = new XLMovementLimiterEvent;
	movLimitEvent->id = id;
	movLimitEvent->axis = Vector3(1, 0, 1);
	engine.addPhysicsEvent(movLimitEvent);
}

void Game::removeEnemy(string id) {
	
	for (int i = 0; i < enemies.size(); ++i) {
		if (enemies.at(i)->getId() == id) {
			spawnDeathParticles(enemies.at(i), enemies.at(i)->getShotBy());
			delete enemies.at(i);
			enemies.erase(enemies.begin() + i);
			break;
		}
	}
	engine.removeEntity(id);
}

void Game::addFloor(float x, float z) {
	string id = "floor" + to_string(floors.size());
	if (engine.getEntity(id)) {
		for (int j = 0; j < floors.size(); ++j) {
			if (!engine.getEntity(string("floor" + to_string(j)))) {
				id = "floor" + to_string(j);
			}
		}
	}
	engine.addEntity(id);
	floors.push_back(engine.getEntity(id));
	engine.setEntityMesh(id, "cube");
	engine.setEntityTexture(id, "floor");
	engine.setEntityRenderScale(id, Vector3(2.0f, 2.0f, 2.0f));
	engine.setEntityRenderPosition(id, Vector3(x, 0, z));
}

void Game::addWall(float x, float z) {
	string id = "wall" + to_string(walls.size());
	if (engine.getEntity(id)) {
		for (int j = 0; j < walls.size(); ++j) {
			if (!engine.getEntity(string("wall" + to_string(j)))) {
				id = "wall" + to_string(j);
			}
		}
	}
	engine.addEntity(id);
	walls.push_back(engine.getEntity(id));
	engine.setEntityMesh(id, "cube");
	engine.setEntityTexture(id, "wall");
	engine.setEntityRenderScale(id, Vector3(2.0f, 2.0f, 2.0f));
	XLNewPhysicsEntityEvent* e = new XLNewPhysicsEntityEvent;
	e->id = id;
	e->shape = PHYSICS_BOX;
	e->position = Vector3(x, 2, z);
	e->mass = 0;
	e->scale = Vector3(1, 1, 1);
	e->gravity = false;
	engine.addPhysicsEvent(e);
}

void Game::addExitWall(float x, float z) {
	string id = "wallExit" + to_string(exitWalls.size());
	if (engine.getEntity(id)) {
		for (int j = 0; j < exitWalls.size(); ++j) {
			if (!engine.getEntity(string("wallExit" + to_string(j)))) {
				id = "wallExit" + to_string(j);
			}
		}
	}
	engine.addEntity(id);
	exitWalls.push_back(engine.getEntity(id));
	engine.setEntityMesh(id, "cube");
	engine.setEntityTexture(id, "wall");
	engine.setEntityRenderScale(id, Vector3(2.0f, 2.0f, 2.0f));
	XLNewPhysicsEntityEvent* e = new XLNewPhysicsEntityEvent;
	e->id = id;
	e->shape = PHYSICS_BOX;
	e->position = Vector3(x, 2, z);
	e->mass = 0;
	e->scale = Vector3(1, 1, 1);
	e->gravity = false;
	engine.addPhysicsEvent(e);
}

void Game::addPlayer(float x, float z) {
	if (!engine.getEntity("player")) {
		string id = "player";
		engine.addEntity(id);

		player.setEntity(engine.getEntity(id));
		player.setId("player");
		player.setHP(playerHealth);
		player.setAlive(true);

		engine.setEntityMesh(id, "cube");
		engine.setEntityTexture(id, "player");
		engine.setEntityRenderScale(id, Vector3(1.0f, 1.0f, 1.0f));
		XLNewPhysicsEntityEvent* e = new XLNewPhysicsEntityEvent;
		e->id = id;
		e->shape = PHYSICS_BOX;
		e->position = Vector3(x, 1.5f, z);
		e->mass = 1;
		e->scale = Vector3(0.5f, 0.5f, 0.5f);
		e->gravity = true;
		engine.addPhysicsEvent(e);

		XLRotationLimiterEvent* rotLimitEvent = new XLRotationLimiterEvent;
		rotLimitEvent->id = "player";
		rotLimitEvent->axis = Vector3(0, 0, 0);
		engine.addPhysicsEvent(rotLimitEvent);

		XLMovementLimiterEvent* movLimitEvent = new XLMovementLimiterEvent;
		movLimitEvent->id = "player";
		movLimitEvent->axis = Vector3(1, 0, 1);
		engine.addPhysicsEvent(movLimitEvent);
	}
}

void Game::addExit(float x, float z) {
	if (!engine.getEntity("exit")) {
		string id = "exit";
		engine.addEntity(id);
		engine.setEntityMesh(id, "cube");
		engine.setEntityTexture(id, "exit");
		engine.setEntityRenderScale(id, Vector3(2.0f, 2.0f, 2.0f));
		XLNewPhysicsEntityEvent* e = new XLNewPhysicsEntityEvent;
		e->id = id;
		e->shape = PHYSICS_BOX;
		e->position = Vector3(x, 2, z);
		e->mass = 0;
		e->scale = Vector3(1, 1, 1);
		e->gravity = true;
		engine.addPhysicsEvent(e);

		XLRotationLimiterEvent* rotLimitEvent = new XLRotationLimiterEvent;
		rotLimitEvent->id = "exit";
		rotLimitEvent->axis = Vector3(0, 1, 0);
		engine.addPhysicsEvent(rotLimitEvent);
	}
}

bool Game::checkCollisionForTags(XLCollisionEvent* e, string s1, string s2) {
	return (e->idA.substr(0, s1.length()) == s1 && e->idB.substr(0, s2.length()) == s2) || (e->idA.substr(0, s2.length()) == s2 && e->idB.substr(0, s1.length()) == s1);
}

void Game::addBullet(float x, float z, Vector3 dir) {
	string id = "bullet" + to_string(bullets.size());
	if (engine.getEntity(id)) {
		for (int j = 0; j < bullets.size(); ++j) {
			if (!engine.getEntity(string("bullet" + to_string(j)))) {
				id = "bullet" + to_string(j);
			}
		}
	}
	engine.addEntity(id);
	Bullet* b = new Bullet;
	b->setEntity(engine.getEntity(id));
	b->setId(id);
	b->setDirection(dir);
	b->setCollided(false);
	b->setDamage(bulletDamage);
	bullets.push_back(b);
	engine.setEntityMesh(id, "sphere");
	engine.setEntityTexture(id, "player");
	engine.setEntityRenderScale(id, Vector3(0.1f, 0.1f, 0.1f));
	XLNewPhysicsEntityEvent* e = new XLNewPhysicsEntityEvent;
	e->id = id;
	e->shape = PHYSICS_SPHERE;
	e->position = Vector3(x, 1.5, z);
	e->mass = 1;
	e->scale = Vector3(0.1f, 0.1f, 0.1f);
	e->gravity = false;
	engine.addPhysicsEvent(e);

	XLRotationLimiterEvent* rotLimitEvent = new XLRotationLimiterEvent;
	rotLimitEvent->id = id;
	rotLimitEvent->axis = Vector3(0, 0, 0);
	engine.addPhysicsEvent(rotLimitEvent);

	XLMovementLimiterEvent* movLimitEvent = new XLMovementLimiterEvent;
	movLimitEvent->id = id;
	movLimitEvent->axis = Vector3(1, 0, 1);
	engine.addPhysicsEvent(movLimitEvent);

	XLImpulseEvent* impE = new XLImpulseEvent;
	impE->id = id;
	Vector3 force = b->getDirection();
	force.y = 0;
	force.Normalise();
	impE->impulse = force * 10;
	impE->impulseLocation = Vector3(0, 0, 0);
	engine.addPhysicsEvent(impE);
}

void Game::removeBullet(string id) {
	for (int i = 0; i < bullets.size(); ++i) {
		if (bullets.at(i)->getId() == id)
			//delete bullets.at(i);
			bullets.erase(bullets.begin() + i);
	}
	engine.removeEntity(id);
}

void Game::spawnDeathParticles(Enemy* enemy, Bullet* bullet) {
	for (int i = 0; i < deathParticleCount; ++i) {
		float angle = (rand() % 100) - 50;
		float xRot = (rand() % 100) - 50;
		float yRot = (rand() % 100);
		Vector3 enemyPos = enemy->getEntity()->getPosition();
		Vector3 imp = enemyPos - bullet->getEntity()->getPosition();

		imp = Matrix4::Rotation(angle, Vector3(xRot, yRot, 0)) * imp;
		imp.Normalise();
		imp = imp / 10;

		string did = "death_" + enemy->getId() + "_" + to_string(deathParticles.size());
		if (engine.getEntity(enemy->getId())) {
			/*for (int j = 0; j < deathParticles.size(); ++j) {
				if (!engine.getEntity(string("death_" + enemy->getId() + "_" + to_string(j)))) {
					did = "death_" + enemy->getId() + "_" + to_string(j);
				}
			}*/
		}

		float xPos = ((rand() % 100) - 50) / 50.0f;
		float yPos = ((rand() % 100) - 50) / 50.0f;
		float zPos = ((rand() % 100) - 50) / 50.0f;

		engine.addEntity(did);
		deathParticles.push_back(engine.getEntity(did));
		Enemy* enemy = new Enemy;
		enemy->setEntity(engine.getEntity(did));
		enemy->setId(did);
		enemy->setHP(1000);
		engine.setEntityMesh(did, "cube");
		engine.setEntityTexture(did, "enemyDeath");
		engine.setEntityRenderScale(did, Vector3(0.2f, 0.2f, 0.2f));
		XLNewPhysicsEntityEvent* e = new XLNewPhysicsEntityEvent;
		e->id = did;
		e->shape = PHYSICS_SPHERE;
		e->position = Vector3(enemyPos.x + xPos, enemyPos.y + yPos, enemyPos.z + zPos);
		e->mass = 0.01f;
		e->scale = Vector3(0.1f, 0.1f, 0.1f);
		e->gravity = true;
		engine.addPhysicsEvent(e);

		XLImpulseEvent* ie = new XLImpulseEvent;
		ie->id = did;
		ie->impulse = imp;
		ie->impulseLocation = Vector3(0, 0, 0);
		engine.addPhysicsEvent(ie);
	}
}

void Game::generateEnemyVisionRays() {
	if (player.isAlive()) {
		for (int i = 0; i < enemies.size(); ++i) {
			XLCastRayAllHitsEvent* e = new XLCastRayAllHitsEvent;
			e->rayId = enemies.at(i)->getId();
			e->start = enemies.at(i)->getEntity()->getPosition();
			e->end = player.getEntity()->getPosition();
			engine.addPhysicsEvent(e);
		}
	}
}

void Game::loadLevel(int level) {
	if (!lastLevel) {
		string file = "level" + to_string(level) + ".txt";

		ifstream f(file.c_str());
		if (!f.good()) {
			//exitGame = true;
			file = "endLevel.txt";
			lastLevel = true;
		}
		f.close();

		XLNewPhysicsEntityEvent* e = new XLNewPhysicsEntityEvent;
		e->id = "floor";
		e->shape = PHYSICS_BOX;
		e->position = Vector3(0, 0, 0);
		e->mass = 0;
		e->scale = Vector3(500, 1, 500);
		e->gravity = false;
		engine.addPhysicsEvent(e);

		string id = "fakeFloor";
		engine.addEntity(id);
		engine.setEntityMesh(id, "cube");
		engine.setEntityTexture(id, "floor");
		engine.setEntityRenderScale(id, Vector3(200.0f, 2.0f, 200.0f));
		engine.setEntityRenderPosition(id, Vector3(0, -0.5, 0));

		FileReader fileReader = engine.getFileReader();
		fileReader.loadFile(file);
		FileData data = fileReader.getDataOfType("MAP_LAYOUT");

		string songFile = bgMusicFile;
		while (songFile == bgMusicFile) {
			int songNum = rand() % 15;
			songFile = "nier";
			songFile = songFile + to_string(songNum);
			songFile = songFile + ".flac";
		}
		bgMusicFile = songFile;
		XL2DPlaySoundEvent* bgMusic = new XL2DPlaySoundEvent(songFile);
		bgMusic->isLooping = true;
		engine.addSoundEvent(bgMusic);

		int column = 0;
		int row = 0;

		addWall(-2, -2);

		for (int i = 0; i < data.getData().length(); ++i) {
			if (column == 0) {
				addWall(-2, row * 2);
			}
			if (row == 0) {
				addWall(column * 2, -2);
			}

			if (data.getData().at(i) != ';') {
				if (data.getData().at(i) != 'h') {
					if (data.getData().at(i) == 'w') {
						addWall(column * 2, row * 2);
					}
					else {
						addFloor(column * 2, row * 2);
					}

					if (data.getData().at(i) == 's') {
						addPlayer(column * 2, row * 2);
					}
					if (data.getData().at(i) == 'm') {
						addEnemy(column * 2, row * 2);
					}
					if (data.getData().at(i) == 'e') {
						addExit(column * 2, row * 2);
					}
					if (data.getData().at(i) == 'b') {
						addExitWall(column * 2, row * 2);
					}
				}
				column++;
			}
			else {
				addWall(column * 2, row * 2);
				row++;
				if (i != data.getData().length() - 1) {
					column = 0;
				}
			}
		}

		for (int i = 0; i < column + 2; ++i) {
			addWall(i * 2 - 2, row * 2);
		}

		levelCleared = false;
	}
	else {
		exitGame = true;
	}
}

void Game::unloadLevel() {

	for (int i = 0; i < enemies.size(); ++i) {
		engine.removeEntity(enemies.at(i)->getId());
		delete enemies.at(i);
	}
	for (int i = 0; i < walls.size(); ++i) {
		engine.removeEntity(walls.at(i)->getId());
	}
	for (int i = 0; i < floors.size(); ++i) {
		engine.removeEntity(floors.at(i)->getId());
	}
	for (int i = 0; i < deathParticles.size(); ++i) {
		engine.removeEntity(deathParticles.at(i)->getId());
	}
	for (int i = 0; i < bullets.size(); ++i) {
		engine.removeEntity(bullets.at(i)->getId());
		delete bullets.at(i);
	}
	for (int i = 0; i < exitWalls.size(); ++i) {
		engine.removeEntity(exitWalls.at(i)->getId());
	}
	enemies.clear();
	walls.clear();
	floors.clear();
	deathParticles.clear();
	bullets.clear();
	exitWalls.clear();
	player.setAlive(false);
	player.setEntity(nullptr);
	engine.removeEntity("player");
	engine.removeEntity("exit");
	engine.removeEntity("fakeFloor");

	XLRemovePhysicsEntityEvent* e = new XLRemovePhysicsEntityEvent;
	e->id = "floor";
	engine.addPhysicsEvent(e);

}

void Game::setup() {
	FileReader fileReader = engine.getFileReader();
	fileReader.loadFile("settings.txt");
	FileData fdDeathParticleCount = fileReader.getDataOfType("DEATH_PARTICLE_COUNT");
	FileData fdPlayerHealth = fileReader.getDataOfType("PLAYER_HEALTH");
	FileData fdEnemyHealth = fileReader.getDataOfType("ENEMY_HEALTH");
	FileData fdBulletDamage = fileReader.getDataOfType("BULLET_DAMAGE");
	FileData fdEnemyDamage = fileReader.getDataOfType("ENEMY_DAMAGE");

	string::size_type sz;

	deathParticleCount = stoi(fdDeathParticleCount.getData(), &sz);
	playerHealth = stoi(fdPlayerHealth.getData(), &sz);
	enemyHealth = stoi(fdEnemyHealth.getData(), &sz);
	bulletDamage = stoi(fdBulletDamage.getData(), &sz);
	enemyDamage = stoi(fdEnemyDamage.getData(), &sz);
}