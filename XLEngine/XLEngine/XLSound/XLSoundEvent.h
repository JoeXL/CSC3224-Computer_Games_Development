#pragma once
#include  <string>
#include "../../nclgl/Vector3.h"
#include "../XLEventQueue/XLEventQueue.h"

enum SoundEventType { PLAY_2D_SOUND, PLAY_3D_SOUND, UNLOAD_SOUND};

using namespace std;

class XLSoundEvent : public XLEvent
{
public:
	XLSoundEvent() {};
	~XLSoundEvent() {};

	SoundEventType getType() { return type; }
	string filename;

protected:
	SoundEventType type;
};

class XLUnloadSoundEvent : public XLSoundEvent {
public:
	XLUnloadSoundEvent(string fname) {
		type = UNLOAD_SOUND;
		filename = fname;
	}
	~XLUnloadSoundEvent() {}
};

class XL2DPlaySoundEvent : public XLSoundEvent {
public:
	XL2DPlaySoundEvent(string fname) {
		type = PLAY_2D_SOUND;
		filename = fname;
	}
	~XL2DPlaySoundEvent() {}

	bool isLooping = false;
	bool isStream = false;
	float volume = 30;
};

class XL3DPlaySoundEvent : public XLSoundEvent {
public:
	XL3DPlaySoundEvent(string fname, Vector3 soundPos, Vector3 camPos, Vector3 camForwards, Vector3 camUp) {
		type = PLAY_3D_SOUND;
		filename = fname;
		soundPosition = soundPos;
		cameraPosition = camPos;
		cameraForwards = camForwards;
		cameraUp = camUp;
	}
	~XL3DPlaySoundEvent() {}

	bool isStream = false;
	Vector3 soundPosition;
	Vector3 cameraPosition;
	Vector3 cameraForwards;
	Vector3 cameraUp;
	float volume = 30;
};