#include "stdafx.h"
#include "XLSound.h"

XLSound::XLSound()
{
	XLSound::errorCheck(FMOD::Studio::System::create(&studioSystem));
	XLSound::errorCheck(studioSystem->initialize(32, FMOD_STUDIO_INIT_LIVEUPDATE, FMOD_INIT_PROFILE_ENABLE, NULL));

	XLSound::errorCheck(studioSystem->getLowLevelSystem(&system));
}


XLSound::~XLSound()
{
	XLSound::errorCheck(studioSystem->unloadAll());
	XLSound::errorCheck(studioSystem->release());
}

void XLSound::update(XLEventQueue* events) {
	/* Process new sound events */
	processEvents(events);

	/* Process loaded sounds */
	vector<map<int, FMOD::Channel*>::iterator> stoppedChannels;
	for (auto it = channels.begin(); it != channels.end(); ++it) {
		bool isPlaying = false;
		it->second->isPlaying(&isPlaying);
		if (!isPlaying)
			stoppedChannels.push_back(it);
	}
	for (auto& it : stoppedChannels) {
		channels.erase(it);
	}
	XLSound::errorCheck(studioSystem->update());
}

void XLSound::loadSound(const string& filename, bool is3D, bool isLooping, bool isStream) {
	string path = SOUNDDIR;
	path += filename;
	auto it = sounds.find(path);
	if (it != sounds.end()) // Check that the sound isn't already loaded
		return;

	FMOD_MODE fmodMode = FMOD_DEFAULT;
	fmodMode |= is3D ? FMOD_3D : FMOD_2D;
	fmodMode |= isLooping ? FMOD_LOOP_NORMAL : FMOD_LOOP_OFF;
	fmodMode |= isStream ? FMOD_CREATESTREAM : FMOD_CREATECOMPRESSEDSAMPLE;

	FMOD::Sound* sound = nullptr;
	XLSound::errorCheck(system->createSound(path.c_str(), fmodMode, nullptr, &sound));
	if (sound) { // If loading the sound succeeded
		sounds[path] = sound;
	}
	else {
		cout << "Could not find sound file: " << path << endl;
	}
}

void XLSound::unloadSound(const string& filename) {
	string path = SOUNDDIR;
	path += filename;
	auto tFoundIt = sounds.find(path);
	if (tFoundIt == sounds.end()) // Check if the sound is loaded
		return;
	XLSound::errorCheck(tFoundIt->second->release());
	sounds.erase(tFoundIt);
}

int XLSound::playSound(const string& filename, float volumedB, const Vector3& cPos, const Vector3& cFwd, const Vector3& cUp, const Vector3& vPos) {
	string path = SOUNDDIR;
	path += filename;
	int channelId = nextChannelId++;
	auto it = sounds.find(path);
	if (it == sounds.end()) { // Check if the sound is loaded
		loadSound(path);
		it = sounds.find(path);
		if (it == sounds.end()) {
			return channelId;
		}
	}
	FMOD::Channel* channel = nullptr;
	XLSound::errorCheck(system->playSound(it->second, nullptr, true, &channel));
	if (channel) { // Set channel settings
		FMOD_MODE mode;
		it->second->getMode(&mode);
		if (mode & FMOD_3D) {
			FMOD_VECTOR position = toFmodVector(vPos);
			XLSound::errorCheck(channel->set3DAttributes(&position, nullptr));
			FMOD_VECTOR pos = toFmodVector(cPos);
			FMOD_VECTOR vel = toFmodVector(Vector3(0, 0, 0));
			FMOD_VECTOR fwd = toFmodVector(cFwd);
			FMOD_VECTOR up = toFmodVector(cUp);
			if(!XLSound::errorCheck(system->set3DListenerAttributes(0, &pos, &vel, &fwd, &up)))
				cout << "Could not find sound file: " << path << endl;
		}
		XLSound::errorCheck(channel->setVolume(dbToVolume(volumedB)));
		XLSound::errorCheck(channel->setPaused(false));
		channels[channelId] = channel;
	}
	return channelId;
}

FMOD_VECTOR XLSound::toFmodVector(const Vector3& vector) {
	FMOD_VECTOR vec;
	vec.x = vector.x;
	vec.y = vector.y;
	vec.z = vector.z;
	return vec;
}

float XLSound::dbToVolume(float dB) {
	return powf(10.0f, 0.05f * dB);
}

float XLSound::VolumeTodb(float volume) {
	return 20.0f * log10f(volume);
}

int XLSound::errorCheck(FMOD_RESULT result) {
	if (result != FMOD_OK) {
		cout << "FMOD ERROR " << result << endl;
		return 1;
	}
	return 0;
}

void XLSound::processEvents(XLEventQueue* events) {
	while (!events->empty()) {
		switch (((XLSoundEvent*)events->front())->getType()) {
		case PLAY_2D_SOUND: {
			XL2DPlaySoundEvent* e = (XL2DPlaySoundEvent*)events->front();
			loadSound(e->filename, false, e->isLooping, e->isStream);
			playSound(e->filename, e->volume);
			delete e;
			break;
		}
		case PLAY_3D_SOUND: {
			XL3DPlaySoundEvent* e = (XL3DPlaySoundEvent*)events->front();
			loadSound(e->filename, true, false, e->isStream);
			playSound(e->filename, e->volume, e->cameraPosition, e->cameraForwards, e->cameraUp, e->soundPosition);
			delete e;
			break;
		}
		case UNLOAD_SOUND: {
			XLUnloadSoundEvent* e = (XLUnloadSoundEvent*)events->front();
			unloadSound(e->filename);
			delete e;
			break;
		}
		}
		events->popEvent();
	}
}