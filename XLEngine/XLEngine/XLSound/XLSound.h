#pragma once
#include "XLSoundEvent.h"
#include "../../nclgl/Vector3.h"
#include "XLSoundEvent.h"
#include "fmod_studio.hpp"
#include "fmod.hpp"
#include <string>
#include <map>
#include <vector>
#include <math.h>
#include <iostream>

using namespace std;

#define SOUNDDIR "../../Sounds/"

class XLSound
{
public:
	XLSound();
	~XLSound();

	void update(XLEventQueue* events);

protected:
	static int errorCheck(FMOD_RESULT result);

	void loadSound(const string& filename, bool b3d = true, bool bLooping = false, bool bStream = false);
	void unloadSound(const string& filename);
	int playSound(const string& filename, float volumedB = 0.0f, const Vector3& cPos = Vector3(0,0,0), const Vector3& cFwd = Vector3(0,0,0), const Vector3& cUp = Vector3(0,0,0), const Vector3& vPos = Vector3(0, 0, 0));

	float dbToVolume(float db);
	float VolumeTodb(float volume);
	FMOD_VECTOR toFmodVector(const Vector3& vector);

	void processEvents(XLEventQueue* events);

private:
	FMOD::Studio::System* studioSystem;
	FMOD::System* system;
	XLEventQueue* soundEvents;
	map<string, FMOD::Sound*> sounds;
	map<int, FMOD::Channel* > channels;
	int nextChannelId;
};

