#pragma once

#include "../../nclgl/Mesh.h"
#include "../../nclgl/SceneNode.h"

class Entity
{
public:
	/* The entity id is used to tie the rendered entity to its partner physics entity */
	Entity(string id) {
		this->id = id;
		renderScale.ToZero();
		renderPosition.ToZero();
		renderRotationAxis.ToZero();
		renderRotationAngle = 0;
	};

	~Entity() {};

	SceneNode* getMesh() { return mesh; }
	void setMesh(SceneNode* sceneNode) { mesh = sceneNode; }

	GLuint getTexture() { return texture; }
	void setTexture(GLuint tex) { 
		texture = tex;
	}

	string getId() { return id; }

	//Matrix4 getTransform() { return transform; }
	Matrix4 getTransform() {
		return Matrix4::Translation(position) * Matrix4::Translation(renderPosition) * Matrix4::Rotation(rotationAngle, rotationAxis) *
			Matrix4::Rotation(renderRotationAngle, renderRotationAxis) * Matrix4::Scale(renderScale);
	}

	/* Set when processing movement events sent from the physics subsystem */
	void setTransform(Matrix4 trans) { transform = trans; }

	void setPosition(Vector3 pos) { position = pos; }
	Vector3 getPosition() { return position; }
	void setRotationAxis(Vector3 axis) { rotationAxis = axis; }
	Vector3 getRotationAxis() { return rotationAxis; }
	void setRotationAngle(float angle) { rotationAngle = angle; }
	float getRotationAngle() { return rotationAngle; }

	Vector3 getRenderScale() { return renderScale; }

	/* Sets the render scale of the object. Has no impact on physics scale */
	void setRenderScale(Vector3 scale) { renderScale = scale; }

	Vector3 getRenderPosition() { return renderPosition; }

	/* Sets the render position of the object. Is an offset from the physics object if one exists */
	void setRenderPosition(Vector3 position) { renderPosition = position; }

	Vector3 getRenderRotationAxis() { return renderRotationAxis; }
	float   getRenderRotationAngle() { return renderRotationAngle; }

	/* Sets the render rotation of the object. Is an offset from the physics object if one exists */
	void setRenderRotation(float angle, Vector3 axis) { renderRotationAngle = angle; renderRotationAxis = axis; }

private:
	string id;
	SceneNode* mesh = nullptr;
	GLuint texture;
	Matrix4 transform;

	Vector3 position = Vector3(0,0,0);
	Vector3 rotationAxis = Vector3(1,0,0);
	float   rotationAngle = 0.0f;

	Vector3 renderScale = Vector3(1,1,1);
	Vector3 renderPosition = Vector3(0,0,0);
	Vector3 renderRotationAxis = Vector3(1,0,0);
	float   renderRotationAngle = 0.0f;
};

