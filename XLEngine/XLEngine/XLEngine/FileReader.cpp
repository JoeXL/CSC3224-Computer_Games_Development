#include "stdafx.h"
#include "FileReader.h"


FileReader::FileReader()
{
}


FileReader::~FileReader()
{
}

FileData FileReader::getDataOfType(string type)
{
	ifstream infile(loadedFile);
	string line;
	bool inType = false;
	FileData fileData;
	string data;
	while (getline(infile, line)) {
		if (inType) {
			if (line == "</" + type + ">") {
				fileData.setData(data);
				break;
			}
			data += line;
		}
		if (line == "<" + type + ">") {
			inType = true;
			fileData.setType(type);
		}
	}

	return fileData;
}

void FileReader::loadFile(string filepath)
{
	loadedFile = filepath;
}

string FileReader::getLoadedFile()
{
	if (isFileLoaded()) {
		return loadedFile;
	}
	else {
		return "";
	}
}
