#pragma once

#include <Windows.h>
#include <iostream>
#include <vector>
#include <map>
#include "../../nclgl/SceneNode.h"
#include "../../nclgl/OBJMesh.h"

using namespace std;

class XLMemoryManager
{
public:
	XLMemoryManager();
	~XLMemoryManager();

	/* Load data into memory */
	void loadTexture(string id, string filename);
	void loadMesh(string id, string filename);

	GLuint getTexture(string id) {
		return textures[id];
	}
	SceneNode* getMesh(string id) {
		return meshes[id];
	}

	bool containsTexture(string id);
	bool containsMesh(string id);

	/* Free memory */
	void unloadMesh(string id);
	void unloadTexture(string id);

private:
	map<string, SceneNode*> meshes;
	map<string, GLuint> textures;
};

