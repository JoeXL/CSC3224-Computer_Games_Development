#pragma once

#pragma once

#include "../../nclgl/Window.h"
#include "Renderer.h"
#include "../XLPhysics/XLPhysics.h"
#include "../XLSound/XLSound.h"
#include "../XLInterface/XLInterface.h"
#include "FileReader.h"
#include "Entity.h"
#include "XLMemoryManager.h"
#include <math.h>

class XLEngine {
public:
	XLEngine();
	virtual ~XLEngine();

	int initialize(string name, int width, int height, bool fullscreen);

	/* Runs a full cycle of the engine. Physics and Interface events will be generated and can then be processed by the game */
	void update();

	Window* getWindow() const { return window; }

	FileReader getFileReader() { return fileReader; }

	void addEntity(string id);
	/* Removes given entity and corresponding physics entity */
	void removeEntity(string id);

	Entity* getEntity(string id);

	/* Sets mesh, texture and render scale for entities of a given id */
	void setEntityMesh(string entityId, string meshName);
	void setEntityTexture(string entityId, string textureName);
	void setEntityRenderScale(string entityId, Vector3 scale);
	void setEntityRenderPosition(string entityId, Vector3 position);
	void setEntityRenderRotation(string entityId, float angle, Vector3 axis);

	void addInterfaceEvent(XLInterfaceEvent* interfaceEvent);
	XLEventQueue* getInterfaceEvents();

	void addPhysicsEvent(XLPhysicsEvent* physicsEvent);
	XLEventQueue* getPhysicsEvents();

	void addSoundEvent(XLSoundEvent* soundEvent);

	/* Load a mesh into memory */
	void loadMesh(string id, string filename) {
		memManager.loadMesh(id, filename);
	}

	/* Load a texture into memory */
	void loadTexture(string id, string filename) {
		memManager.loadTexture(id, filename);
	}

	/* Frees the memory of a mesh. Only unloads if it is not being used by an entity */
	bool unloadMesh(string id) {
		for (int i = 0; i < entities.size(); ++i) {
			if (entities[i]->getMesh() == memManager.getMesh(id))
				return false;
		}
		memManager.unloadMesh(id);
		return true;
	}

	/* Frees the memory of a texture. Only unloads if it is not being used by an entity */
	bool unloadTexture(string id) {
		for (int i = 0; i < entities.size(); ++i) {
			if (entities[i]->getTexture() == memManager.getTexture(id))
				return false;
		}
		memManager.unloadTexture(id);
		return true;
	}

	float degToRad(float angle) {
		return angle * (M_PI / 180);
	}

	Vector3 frontFromEuler(float yaw, float pitch) {
		float x = sin(degToRad(yaw)) * cos(degToRad(pitch));
		float y = sin(degToRad(pitch));
		float z = cos(degToRad(yaw)) * cos(degToRad(pitch));
		Vector3 front(x, y, z);
		front.Normalise();
		return front;
	}

	/* Camera data getters */
	Vector3 getCameraPosition();
	float getCameraPitch();
	float getCameraYaw();

	/* Camera data setters */
	void setCameraPosition(Vector3 position);
	void setCameraPitch(float pitch);
	void setCameraYaw(float yaw);

	Vector2 getScreenSize() { return window->GetScreenSize(); }

private:
	Window* window;
	Renderer* renderer;
	XLPhysics physics;
	XLSound sound;
	XLInterface inter;
	FileReader fileReader;
	XLEventQueue* interfaceEvents;
	XLEventQueue* physicsEvents;
	XLEventQueue* soundEvents;
	vector<Entity*> entities;
	XLMemoryManager memManager;
};