#pragma once
#include "stdafx.h"

#include "XLEngine.h"

XLEngine::XLEngine() {
}

XLEngine::~XLEngine() {}

int XLEngine::initialize(string name, int width, int height, bool fullscreen) {
	window = new Window(name, width, height, fullscreen);
	if (!window->HasInitialised()) {
		return -1;
	}

	renderer = new Renderer(*window);
	if (!renderer->HasInitialised()) {
		return -1;
	}

	window->LockMouseToWindow(false);
	window->ShowOSPointer(true);

	interfaceEvents = new XLEventQueue;
	physicsEvents = new XLEventQueue;
	soundEvents = new XLEventQueue;

	inter.initialize(Window::GetKeyboard(), Window::GetMouse());

	return 0;
}

void XLEngine::update() {


	/* Update interface, physics and sound. Renderer updated after physics events are processed so that new entity positions can be set */
	float preInterTime = window->GetTimer()->GetMS();
	inter.update(window->GetTimer()->GetTimedMS(), interfaceEvents);
	float interfaceTime = window->GetTimer()->GetMS() - preInterTime;

	float preRenderTime = window->GetTimer()->GetMS();
	/* Update and render the scene */
	renderer->UpdateScene(window->GetTimer()->GetTimedMS(), entities);
	renderer->RenderScene();
	float rendererTime = window->GetTimer()->GetMS() - preRenderTime;

	float prePhysiscTime = window->GetTimer()->GetMS();
	physics.update(window->GetTimer()->GetMS(), physicsEvents);
	float physicsTime = window->GetTimer()->GetMS() - prePhysiscTime;

	float preSoundTime = window->GetTimer()->GetMS();
	sound.update(soundEvents);
	float soundTime = window->GetTimer()->GetMS() - preSoundTime;

	for (int i = 0; i < entities.size(); ++i) {
		Entity* ent = entities.at(i);
		if (ent->getMesh()) {
			ent->setTransform(Matrix4::Translation(ent->getRenderPosition()) * Matrix4::Rotation(ent->getRenderRotationAngle(), ent->getRenderRotationAxis()) * Matrix4::Scale(ent->getRenderScale()));
		}
	}

	/* Process the generated physics events */
	XLEventQueue* genPhysEvents = physics.getGeneratedEvents();
	while (!genPhysEvents->empty()) {
		XLPhysicsEvent* physicsEvent = (XLPhysicsEvent*)genPhysEvents->front();
		switch (physicsEvent->getType()) {
		case POSITION: { // Update the transformtation of the entity to match corresponding physics entity
			XLPositionEvent* e = (XLPositionEvent*)genPhysEvents->front();
			Entity* ent = getEntity(e->id);
			if (ent) {
				//Vector3 trans(e->position);
				//Matrix4 rot = Matrix4::Rotation(e->rotationAngle, e->rotationAxis);
				if (ent->getMesh()) {
					//ent->setTransform(Matrix4::Translation(trans) * Matrix4::Translation(ent->getRenderPosition()) * rot * ent->getRenderRotation() * Matrix4::Scale(ent->getRenderScale()));
					//ent->setTransform(Matrix4::Translation(trans) * rot * Matrix4::Scale(ent->getRenderScale()));
					ent->setPosition(e->position);
					ent->setRotationAxis(e->rotationAxis);
					ent->setRotationAngle(e->rotationAngle);
				}
			}
			delete e;
			break;
		}
		case COLLISION: { // Copy the collision event to the event queue for the game to look at
			XLCollisionEvent* e = (XLCollisionEvent*)genPhysEvents->front();
			physicsEvents->push(new XLCollisionEvent(*e));
			delete e;
			break;
		}
		case RETURN_VELOCITY: {// Copy the velocity return event to the event queue for the game to look at
			XLReturnVelocityEvent* e = (XLReturnVelocityEvent*)genPhysEvents->front();
			physicsEvents->push(new XLReturnVelocityEvent(*e));
			delete e;
			break;
		}
		case RETURN_RAY_INFO: {// Copy the ray return event to the event queue for the game to look at
			XLReturnRayEvent* e = (XLReturnRayEvent*)genPhysEvents->front();
			physicsEvents->push(new XLReturnRayEvent(*e));
			delete e;
			break;
		}
		case RETURN_RAY_ALL_HITS_INFO: {// Copy the ray all hits return event to the event queue for the game to look at
			XLReturnRayAllHitsEvent*e = (XLReturnRayAllHitsEvent*)genPhysEvents->front();
			physicsEvents->push(new XLReturnRayAllHitsEvent(*e));
			delete e;
		}
		}
		genPhysEvents->popEvent();
	}

	/* Set the interface event queue for the game to look at */
	interfaceEvents = inter.getGeneratedEvents();

	/* Print out the processing time for each subsystem if P is pressed*/
	if (window->GetKeyboard()->KeyTriggered(KEYBOARD_P)) {
		cout << "Interface Time: " << interfaceTime << "ms Physics Time: " << physicsTime << "ms Sound Time: " << soundTime << "ms Renderer Time: " << rendererTime << "ms" << endl;
	}
}

void XLEngine::addEntity(string id) {
	for (int i = 0; i < entities.size(); ++i) { // Check if entity already exists
		if (entities[i]->getId() == id) {
			cout << "Entity already exists with id: " << id << endl;
			return;
		}
	}
	Entity* temp = new Entity(id);
	entities.push_back(temp);
}

void XLEngine::removeEntity(string id) {
	/* Tell the physics project to delete the corresponding entity */
	XLRemovePhysicsEntityEvent* e = new XLRemovePhysicsEntityEvent;
	e->id = id;
	physicsEvents->push(e);

	/* Delete the entity */
	for (int i = 0; i < entities.size(); ++i) {
		if (entities[i]->getId() == id) {
			delete entities[i];
			entities.erase(entities.begin() + i);
			break;
		}
	}
}

Entity* XLEngine::getEntity(string id) {
	for (int i = 0; i < entities.size(); ++i) {
		if (entities[i]->getId() == id)
			return entities[i];
	}
	//cout << "No entity of name: " << id << endl;
	return nullptr;
}

void XLEngine::setEntityMesh(string entityId, string meshName) {
	getEntity(entityId)->setMesh(memManager.getMesh(meshName));
}

void XLEngine::setEntityTexture(string entityId, string textureName) {
	getEntity(entityId)->setTexture(memManager.getTexture(textureName));
}

void XLEngine::setEntityRenderScale(string entityId, Vector3 scale) {
	getEntity(entityId)->setRenderScale(scale);
}

void XLEngine::setEntityRenderPosition(string entityId, Vector3 position) {
	getEntity(entityId)->setRenderPosition(position);
}

void XLEngine::setEntityRenderRotation(string entityId, float angle, Vector3 axis) {
	getEntity(entityId)->setRenderRotation(angle, axis);
}

void XLEngine::addPhysicsEvent(XLPhysicsEvent* physicsEvent) {
	physicsEvents->push(physicsEvent);
}

XLEventQueue* XLEngine::getPhysicsEvents() {
	return physicsEvents;
}

void XLEngine::addInterfaceEvent(XLInterfaceEvent* interfaceEvent) {
	interfaceEvents->push(interfaceEvent);
}

XLEventQueue* XLEngine::getInterfaceEvents() {
	return interfaceEvents;
}

void XLEngine::addSoundEvent(XLSoundEvent* soundEvent) {
	soundEvents->push(soundEvent);
}

Vector3 XLEngine::getCameraPosition() {
	return renderer->getCamera()->GetPosition();
}

float XLEngine::getCameraPitch() {
	return renderer->getCamera()->GetPitch();
}

float XLEngine::getCameraYaw() {
	return renderer->getCamera()->GetYaw();
}

void XLEngine::setCameraPosition(Vector3 position) {
	renderer->getCamera()->SetPosition(position);
}

void XLEngine::setCameraPitch(float pitch) {
	renderer->getCamera()->SetPitch(pitch);
}

void XLEngine::setCameraYaw(float yaw) {
	renderer->getCamera()->SetYaw(yaw);
}