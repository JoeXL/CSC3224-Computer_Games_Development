#pragma once

#include <string>

using namespace std;

class FileData
{
public:
	FileData();
	FileData(string type, string data) { this->type = type; this->data = data; }
	~FileData();

	void setType(string s) { type = s; }
	string getType() { return type; }

	void setData(string s) { data = s; }
	string getData() { return data; }

private:
	string type;
	string data;
};

