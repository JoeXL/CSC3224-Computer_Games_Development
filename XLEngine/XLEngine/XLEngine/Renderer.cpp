#include "stdafx.h"
#include "Renderer.h"

Renderer::Renderer(Window& parent) : OGLRenderer(parent) {

	camera = new Camera(0.0f, 135.0f, Vector3(0, 0, 0));

	baseShader = new Shader(SHADERDIR"texturedVertex.glsl",
		SHADERDIR"texturedFragment.glsl");

	if (!baseShader->LinkProgram()) {
		return;
	}

	projMatrix = Matrix4::Perspective(1.0f, 15000.0f, (float)width / (float)height, 45.0f);

	glEnable(GL_DEPTH_TEST);
	init = true;
}

Renderer::~Renderer(void) {
	currentShader = NULL;
	delete baseShader;
	delete camera;
}

void Renderer::UpdateScene(float msec, vector<Entity*> ents) {
	entities = ents;
	camera->UpdateCamera(msec);
	viewMatrix = camera->BuildViewMatrix();
}

void Renderer::RenderScene() {
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

	currentShader = baseShader;

	glUseProgram(currentShader->GetProgram());
	glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "diffuseTex"), 0);

	glUniform3fv(glGetUniformLocation(currentShader->GetProgram(),
		"cameraPos"), 1, (float*)&camera->GetPosition());

	UpdateShaderMatrices();

	for (int i = 0; i < entities.size(); ++i) {
		if (entities[i]->getMesh()) {
			glUniformMatrix4fv(glGetUniformLocation(
				currentShader->GetProgram(), "modelMatrix"), 1, false, (float*)&(entities[i]->getTransform()));
			entities[i]->getMesh()->GetMesh()->SetTexture(entities[i]->getTexture());
			glBindTexture(GL_TEXTURE_2D, entities[i]->getMesh()->GetMesh()->GetTexture());
			entities[i]->getMesh()->Draw();
		}
	}

	glUseProgram(0);

	SwapBuffers();
}

Camera* Renderer::getCamera() {
	return camera;
}