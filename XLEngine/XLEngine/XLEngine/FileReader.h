#pragma once

#include <string>
#include <vector>
#include "FileData.h"
#include <fstream>
#include <sstream>

using namespace std;

static class FileReader
{
public:
	FileReader();
	~FileReader();

	/* Returns all data between the tag specified */
	FileData getDataOfType(string type);

	/* Loads a given file. This file will then be used when calling future functions */
	void loadFile(string filepath);
	string getLoadedFile();
	bool isFileLoaded() { return fileLoaded; }

private:
	bool fileLoaded;
	string loadedFile;
	string data;
};

