#pragma once

#include "../../nclgl/OGLRenderer.h"
#include "../../nclgl/HeightMap.h"
#include "../../nclgl/Camera.h"
#include "../../nclgl/OBJMesh.h"
#include "../../nclgl/SceneNode.h"
#include "Entity.h"

class Renderer : public OGLRenderer {
public:
	Renderer(Window& parent);
	virtual ~Renderer(void);

	/* Updates the entities list, camera and subsequently the viewmatrix */
	virtual void UpdateScene(float msec, vector<Entity*> ents);

	/* Renders the scene based on the entities loaded during UpdateScene*/
	virtual void RenderScene();

	Camera* getCamera();

protected:
	Shader* baseShader;
	Camera* camera;

private:
	vector<Entity*> entities;
};