#include "stdafx.h"
#include "XLMemoryManager.h"


XLMemoryManager::XLMemoryManager()
{
}


XLMemoryManager::~XLMemoryManager()
{
}

void XLMemoryManager::loadTexture(string id, string filename) {
	string path = TEXTUREDIR;
	path += filename;
	GLuint tex = SOIL_load_OGL_texture(path.c_str(), SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS);
	if (tex != 0) { //If creation succeeded
		textures.insert(make_pair(id, tex));
	}
	else {
		cout << "Error loading texture: " << path << endl;
	}
}

void XLMemoryManager::loadMesh(string id, string filename) {
	string path = MESHDIR;
	path += filename;
	OBJMesh* objMesh = new OBJMesh;
	if (objMesh->LoadOBJMesh(path)) { //If creation succeeded
		SceneNode* mesh = new SceneNode((Mesh*)objMesh);
		meshes.insert(make_pair(id, mesh));
	}
	else {
		delete objMesh;
		cout << "Error loading mesh: " << path << endl;
	}
}

bool XLMemoryManager::containsTexture(string id) {
	map<string, GLuint>::iterator it;

	for (it = textures.begin(); it != textures.end(); it++)
	{
		if (it->first == id)
			return true;
	}
	return false;
}

bool XLMemoryManager::containsMesh(string id) {
	map<string, SceneNode*>::iterator it;

	for (it = meshes.begin(); it != meshes.end(); it++)
	{
		if (it->first == id)
			return true;
	}
	return false;
}

void XLMemoryManager::unloadMesh(string id) {
	if (containsMesh(id)) {
		delete meshes[id];
		meshes.erase(id);
	}
}

void XLMemoryManager::unloadTexture(string id) {
	if (containsTexture(id)) {
		glDeleteTextures(1, &textures[id]);
		textures.erase(id);
	}
}