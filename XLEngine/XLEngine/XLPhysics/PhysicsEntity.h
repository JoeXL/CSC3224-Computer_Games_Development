#pragma once
#include <string>
#include "btBulletDynamicsCommon.h"

using namespace std;

enum cShape { PHYSICS_BOX, PHYSICS_SPHERE, PHYSICS_BOUNDING_PLANE,
			  PHYSICS_CYLINDER, PHYSICS_CAPSULE, PHYSICS_CONE};

class PhysicsEntity
{
public:
	PhysicsEntity(string parentId, cShape shape, const btVector3& position, const btQuaternion& rotation, const btVector3& scale, float mass, bool gravity);
	~PhysicsEntity();

	string getId() {
		return parentId;
	}

	void setMass(float mass) { this->mass = mass; }
	const float getMass() const { return mass; }

	void setGravity(bool gravity) { this->gravity = gravity; }
	const bool getGravity() const { return gravity; }

	btRigidBody* getRigidBody();

	btVector3 getPosition();

	btQuaternion getRotation();

	void reposition(const btVector3& position, const btQuaternion& orientation);

private:
	string parentId;
	float mass;
	bool gravity;
	cShape shapeType;

	btCollisionShape* collisionShape;
	btDefaultMotionState* motionState;
	btRigidBody* rigidBody;
};

