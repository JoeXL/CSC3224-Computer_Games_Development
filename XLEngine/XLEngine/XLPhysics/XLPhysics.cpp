#include "stdafx.h"
#include "XLPhysics.h"


XLPhysics::XLPhysics()
{
	eventQueue = new XLEventQueue;

	gravity = -10;

	broadphase = new btDbvtBroadphase();
	collisionConfiguration = new btDefaultCollisionConfiguration();
	dispatcher = new btCollisionDispatcher(collisionConfiguration);
	solver = new btSequentialImpulseConstraintSolver;
	dynamicsWorld = new btDiscreteDynamicsWorld(dispatcher, broadphase, solver, collisionConfiguration);
	dynamicsWorld->setGravity(btVector3(0, gravity, 0));
}


XLPhysics::~XLPhysics()
{
	delete dynamicsWorld;
	delete dispatcher;
	delete solver;
	delete collisionConfiguration;
	delete broadphase;
}

void XLPhysics::update(float msec, XLEventQueue* events)
{
	/* Process new physics events */
	processEvents(events);

	/* Apply a force opposite to gravity if an entity is marked as not having gravity */
	for (int i = 0; i < entities.size(); ++i) {
		if (!entities[i]->getGravity()) {
			entities[i]->getRigidBody()->applyForce(btVector3(0, -gravity, 0), btVector3(0, 0, 0));
		}
	}

	float timeStep = (msec - msLastCall) / 1000.0;
	msLastCall = msec;

	int subSteps = timeStep / 0.017f;
	subSteps += 1.0f;
	subSteps *= (1 + (1.0f / subSteps));

	dynamicsWorld->stepSimulation(timeStep, subSteps);

	/* Generate movement events that are to be used by the renderer */
	generateMovementEvents();
	/* Generate collision events that can be used by the game */
	generateCollisionEvents();
}

void XLPhysics::addEntity(string id, cShape shape, const btVector3& position, const btQuaternion& rotation, const btVector3& scale, float mass, bool gravity) {
	PhysicsEntity* temp = new PhysicsEntity(id, shape, position, rotation, scale, mass, gravity);
	btRigidBody* tempBody = temp->getRigidBody();
	dynamicsWorld->addRigidBody(tempBody);
	entities.push_back(temp);
}

void XLPhysics::removeEntity(string id) {
	for (int i = 0; i < entities.size(); ++i) {
		if (entities[i]->getId() == id) {
			dynamicsWorld->removeRigidBody(entities[i]->getRigidBody());
			delete entities[i];
			entities.erase(entities.begin() + i);
		}
	}
}

PhysicsEntity* XLPhysics::getEntity(string id)
{
	for (int i = 0; i < entities.size(); ++i) {
		if (entities[i]->getId() == id)
			return entities[i];
	}
	cout << "No physics entity named: " << id << endl;
	return nullptr;
}

PhysicsEntity* XLPhysics::getEntity(btRigidBody* entity) {
	for (int i = 0; i < entities.size(); ++i) {
		if (entity == entities[i]->getRigidBody())
			return entities[i];
	}
	return nullptr;
}

void XLPhysics::generateMovementEvents() {
	for (int i = 0; i < entities.size(); ++i) {
		XLPositionEvent* posEvent = new XLPositionEvent;
		posEvent->id = entities[i]->getId();
		posEvent->position = fromBulletVec(entities[i]->getPosition());
		posEvent->rotationAxis = fromBulletVec(entities[i]->getRotation().getAxis());
		posEvent->rotationAngle = radToDeg(entities[i]->getRotation().getAngle());
		eventQueue->push(posEvent);
	}
}

void XLPhysics::generateCollisionEvents() {
	int numManifolds = dynamicsWorld->getDispatcher()->getNumManifolds();
	for (int i = 0; i < numManifolds; ++i) {
		btPersistentManifold* contactManifold = dynamicsWorld->getDispatcher()->getManifoldByIndexInternal(i);

		int numContacts = contactManifold->getNumContacts();
		if (numContacts != 0) { // If there is a contact between the pair of bodies, generate a collision event
			XLCollisionEvent* e = new XLCollisionEvent;
			e->idA = getEntity((btRigidBody*)contactManifold->getBody0())->getId();
			e->idB = getEntity((btRigidBody*)contactManifold->getBody1())->getId();
			for (int j = 0; j < numContacts; ++j) {
				btManifoldPoint& pt = contactManifold->getContactPoint(j);
				if (pt.getDistance() < 0.0f) {
					const btVector3& ptA = pt.getPositionWorldOnA();
					const btVector3& ptB = pt.getPositionWorldOnB();
					const btVector3& normalOnB = pt.m_normalWorldOnB;
					e->collisionPointsOnA.push_back(fromBulletVec(pt.getPositionWorldOnA()));
					e->collisionPointsOnB.push_back(fromBulletVec(pt.getPositionWorldOnB()));
					e->normalWorldOnB.push_back(fromBulletVec(pt.m_normalWorldOnB));
				}
			}
			eventQueue->push(e);
		}
	}
}

void XLPhysics::applyForce(string id, const btVector3& force, const btVector3& forceLocation) {
	if (PhysicsEntity* entity = getEntity(id)) {
		entity->getRigidBody()->applyForce(force, forceLocation);
		entity->getRigidBody()->activate();
	}
}

void XLPhysics::applyImpulse(string id, const btVector3& impulse, const btVector3& impulseLocation) {
	if (PhysicsEntity* entity = getEntity(id)) {
		entity->getRigidBody()->applyImpulse(impulse, impulseLocation);
		entity->getRigidBody()->activate();
	}
}

void XLPhysics::applyTorque(string id, const btVector3& torque) {
	if (PhysicsEntity* entity = getEntity(id)) {
		entity->getRigidBody()->applyTorque(torque);
		entity->getRigidBody()->activate();
	}
}

void XLPhysics::applyAngularForce(string id, const btVector3& force) {
	if (PhysicsEntity* entity = getEntity(id)) {
		entity->getRigidBody()->setAngularVelocity(force);
		entity->getRigidBody()->activate();
	}
}

void XLPhysics::rotateEntity(string id, const btQuaternion& rotation) {
	if (PhysicsEntity* entity = getEntity(id)) {
		btTransform trans(rotation);
		entity->getRigidBody()->setCenterOfMassTransform(entity->getRigidBody()->getCenterOfMassTransform() * trans);
		entity->getRigidBody()->activate();
	}
}

void XLPhysics::translateEntity(string id, const btVector3& translation) {
	if (PhysicsEntity* entity = getEntity(id)) {
		entity->getRigidBody()->translate(translation);
		entity->getRigidBody()->activate();
	}
}

void XLPhysics::setEntityPosition(string id, const btVector3& position) {
	if (PhysicsEntity* entity = getEntity(id)) {
		entity->getRigidBody()->getWorldTransform().setOrigin(position);
		entity->getRigidBody()->activate();
	}
}

void XLPhysics::setEntityRotation(string id, const btQuaternion& rotation) {
	if (PhysicsEntity* entity = getEntity(id)) {
		entity->getRigidBody()->getWorldTransform().setRotation(rotation);
		entity->getRigidBody()->activate();
	}
}

void XLPhysics::setLinearFactor(string id, const btVector3& axis) {
	if (PhysicsEntity* entity = getEntity(id)) {
		entity->getRigidBody()->setLinearFactor(axis);
	}
}

void XLPhysics::setAngularFactor(string id, const btVector3& axis) {
	if (PhysicsEntity* entity = getEntity(id)) {
		entity->getRigidBody()->setAngularFactor(axis);
	}
}

btCollisionWorld::ClosestRayResultCallback XLPhysics::castRay(const btVector3& start, const btVector3& end) {
	btCollisionWorld::ClosestRayResultCallback results(start, end);
	dynamicsWorld->rayTest(start, end, results);
	return results;
}

btCollisionWorld::AllHitsRayResultCallback XLPhysics::castRayAllHits(const btVector3& start, const btVector3& end) {
	btCollisionWorld::AllHitsRayResultCallback results(start, end);
	dynamicsWorld->rayTest(start, end, results);
	return results;
}

void XLPhysics::processEvents(XLEventQueue* events) {
	while (!events->empty()) {
		switch (((XLPhysicsEvent*)events->front())->getType()) {
		case NEW_PHYSICS_ENTITY: {
			XLNewPhysicsEntityEvent* e = (XLNewPhysicsEntityEvent*)events->front();
			addEntity(e->id, e->shape, toBulletVec(e->position), toBulletQuaternion(e->rotationAngle, e->rotationAxis), toBulletVec(e->scale), e->mass, e->gravity);
			delete e;
			break;
		}
		case REMOVE_PHYSICS_ENTITY: {
			XLRemovePhysicsEntityEvent* e = (XLRemovePhysicsEntityEvent*)events->front();
			removeEntity(e->id);
			delete e;
			break;
		}
		case FORCE: {
			XLForceEvent* e = (XLForceEvent*)events->front();
			applyForce(e->id, toBulletVec(e->force), toBulletVec(e->forceLocation));
			delete e;
			break;
		}
		case IMPULSE: {
			XLImpulseEvent* e = (XLImpulseEvent*)events->front();
			applyImpulse(e->id, toBulletVec(e->impulse), toBulletVec(e->impulseLocation));
			delete e;
			break;
		}
		case LIMIT_MOVEMENT: {
			XLMovementLimiterEvent* e = (XLMovementLimiterEvent*)events->front();
			setLinearFactor(e->id, toBulletVec(e->axis));
			delete e;
			break;
		}
		case LIMIT_ROTATION: {
			XLRotationLimiterEvent* e = (XLRotationLimiterEvent*)events->front();
			setAngularFactor(e->id, toBulletVec(e->axis));
			delete e;
			break;
		}
		case GET_VELOCITY: {
			XLGetVelocityEvent* e = (XLGetVelocityEvent*)events->front();
			XLReturnVelocityEvent* re = new XLReturnVelocityEvent;
			re->id = e->id;
			re->velocity = fromBulletVec(getEntity(e->id)->getRigidBody()->getLinearVelocity());
			eventQueue->push(re);
			delete e;
			break;
		}
		case POSITION: {
			XLPositionEvent* e = (XLPositionEvent*)events->front();
			delete e;
			break;
		}
		case COLLISION: {
			XLCollisionEvent* e = (XLCollisionEvent*)events->front();
			delete e;
			break;
		}
		case RETURN_VELOCITY: {
			XLReturnVelocityEvent* e = (XLReturnVelocityEvent*)events->front();
			delete e;
			break;
		}
		case TORQUE: {
			XLTorqueEvent* e = (XLTorqueEvent*)events->front();
			applyTorque(e->id, toBulletVec(e->torque));
			delete e;
			break;
		}
		case ANGULAR_FORCE: {
			XLAngularForceEvent* e = (XLAngularForceEvent*)events->front();
			applyAngularForce(e->id, toBulletVec(e->force));
			delete e;
			break;
		}
		case TRANSFORM_ROTATE: {
			XLRotateEvent* e = (XLRotateEvent*)events->front();
			rotateEntity(e->id, toBulletQuaternion(e->yaw, e->pitch, e->roll));
			delete e;
			break;
		}
		case TRANSFORM_TRANSLATE: {
			XLTranlateEvent* e = (XLTranlateEvent*)events->front();
			translateEntity(e->id, toBulletVec(e->translation));
			delete e;
			break;
		}
		case SET_POSITION: {
			XLSetPositionEvent* e = (XLSetPositionEvent*)events->front();
			setEntityPosition(e->id, toBulletVec(e->position));
			delete e;
			break;
		}
		case SET_ROTATION: {
			XLSetRotationEvent* e = (XLSetRotationEvent*)events->front();
			setEntityRotation(e->id, btQuaternion(toBulletVec(e->axis), degToRad(e->angle)));
			delete e;
			break;
		}
		case CAST_RAY: {
			XLCastRayEvent* e = (XLCastRayEvent*)events->front();
			btCollisionWorld::ClosestRayResultCallback rayResults = castRay(toBulletVec(e->start), toBulletVec(e->end));
			XLReturnRayEvent* re = new XLReturnRayEvent;
			re->rayId = e->rayId;
			re->start = e->start;
			re->end = e->end;
			if (rayResults.hasHit()) {
				re->hasHit = true;
				re->hitPoint = fromBulletVec(rayResults.m_hitPointWorld);
				re->hitEntityId = getEntity((btRigidBody*)rayResults.m_collisionObject)->getId();
			}
			eventQueue->push(re);
			delete e;
			break;
		}
		case RETURN_RAY_INFO: {
			XLReturnRayEvent* e = (XLReturnRayEvent*)events->front();
			delete e;
			break;
		}
		case CAST_RAY_ALL_HITS: {
			XLCastRayAllHitsEvent* e = (XLCastRayAllHitsEvent*)events->front();
			btCollisionWorld::AllHitsRayResultCallback rayResults = castRayAllHits(toBulletVec(e->start), toBulletVec(e->end));
			XLReturnRayAllHitsEvent* re = new XLReturnRayAllHitsEvent;
			re->rayId = e->rayId;
			re->start = e->start;
			re->end = e->end;
			if (rayResults.hasHit()) {
				re->hasHit = true;
				vector<Vector3> points;
				vector<string> ids;
				for (int i = 0; i < rayResults.m_hitPointWorld.size(); ++i) {
					points.push_back(fromBulletVec(rayResults.m_hitPointWorld.at(i)));
					ids.push_back(getEntity((btRigidBody*)rayResults.m_collisionObjects.at(i))->getId());
				}
				re->hitPoints = points;
				re->hitEntityIds = ids;
			}
			eventQueue->push(re);
			delete e;
			break;
		}
		case RETURN_RAY_ALL_HITS_INFO: {
			XLReturnRayAllHitsEvent* e = (XLReturnRayAllHitsEvent*)events->front();
			delete e;
			break;
		}
		}
		events->popEvent();
	}
}