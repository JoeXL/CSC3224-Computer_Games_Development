#pragma once
#define _USE_MATH_DEFINES

#include "btBulletDynamicsCommon.h"
#include "XLPhysicsEvent.h"
#include <iostream>
#include <vector>
#include "PhysicsEntity.h"
#include "../../nclgl/Vector3.h"
#include <math.h>

class XLPhysics
{
public:
	XLPhysics();
	~XLPhysics();

	void update(float msec, XLEventQueue* events);

	XLEventQueue* getGeneratedEvents() { return eventQueue; }

protected:
	void processEvents(XLEventQueue* events);

	void setGravity(float grav) { gravity = grav; }
	const float getGravity() { return gravity; }

	void addEntity(string id, cShape shape, const btVector3& position, const btQuaternion& rotation, const btVector3& scale, float mass, bool gravity);
	void removeEntity(string id);

	/* Generate an event for each entity that holds its position and rotation */
	void generateMovementEvents();
	/* Generate an event for each collision pair */
	void generateCollisionEvents();

	PhysicsEntity* getEntity(string id);
	PhysicsEntity* getEntity(btRigidBody* entity);

	void applyForce(string id, const btVector3& force, const btVector3& forceLocation);
	void applyImpulse(string id, const btVector3& impulse, const btVector3& impulseLocation);
	void applyTorque(string id, const btVector3& torque);
	void applyAngularForce(string id, const btVector3& force);

	void rotateEntity(string id, const btQuaternion& rotation);
	void translateEntity(string id, const btVector3& translation);
	void setEntityPosition(string id, const btVector3& position);
	void setEntityRotation(string id, const btQuaternion& rotation);

	void setLinearFactor(string id, const btVector3& axis);
	void setAngularFactor(string id, const btVector3& axis);

	btCollisionWorld::ClosestRayResultCallback castRay(const btVector3& start, const btVector3& end);
	btCollisionWorld::AllHitsRayResultCallback castRayAllHits(const btVector3& start, const btVector3& end);


	btVector3 toBulletVec(const Vector3& vec) {
		return btVector3(vec.x, vec.y, vec.z);
	}

	Vector3 fromBulletVec(const btVector3& vec) {
		return Vector3(vec.x(), vec.y(), vec.z());
	}

	btScalar degToRad(const btScalar& angle) {
		return angle * (M_PI / 180);
	}

	btScalar radToDeg(const btScalar& angle) {
		return angle * (180 / M_PI);
	}

	btQuaternion toBulletQuaternion(float angleDeg, const Vector3& vector) {
		return btQuaternion(toBulletVec(vector), degToRad(angleDeg));
	}

	btQuaternion toBulletQuaternion(float yaw, float pitch, float roll) {
		return btQuaternion(degToRad(yaw), degToRad(pitch), degToRad(roll));
	}

private:
	vector<PhysicsEntity*> entities;
	XLEventQueue* eventQueue;

	float gravity;

	btBroadphaseInterface* broadphase;
	btDefaultCollisionConfiguration* collisionConfiguration;
	btCollisionDispatcher* dispatcher;
	btSequentialImpulseConstraintSolver* solver;
	btDiscreteDynamicsWorld* dynamicsWorld;

	float msLastCall = 0;
};

