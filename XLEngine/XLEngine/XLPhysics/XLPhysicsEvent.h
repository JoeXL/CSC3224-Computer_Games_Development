#pragma once
#include "../XLEventQueue/XLEventQueue.h"
#include <string>
#include "btBulletDynamicsCommon.h"
#include "../../nclgl/Vector3.h"
#include "PhysicsEntity.h"

enum PhysicsEventType { NEW_PHYSICS_ENTITY, REMOVE_PHYSICS_ENTITY, FORCE, IMPULSE, POSITION, COLLISION,
						LIMIT_MOVEMENT, LIMIT_ROTATION, CAST_RAY, RETURN_RAY_INFO, GET_VELOCITY, RETURN_VELOCITY, TORQUE,
						ANGULAR_FORCE, TRANSFORM_ROTATE, TRANSFORM_TRANSLATE, SET_POSITION, SET_ROTATION,
					    CAST_RAY_ALL_HITS, RETURN_RAY_ALL_HITS_INFO };

using namespace std;

class XLPhysicsEvent : public XLEvent
{
public:
	XLPhysicsEvent() {}
	~XLPhysicsEvent() {}

	PhysicsEventType getType() { return type; }

protected:
	PhysicsEventType type;
};

class XLNewPhysicsEntityEvent : public XLPhysicsEvent {
public:
	XLNewPhysicsEntityEvent() { type = NEW_PHYSICS_ENTITY; }
	~XLNewPhysicsEntityEvent() {}

	string id;
	cShape shape;
	Vector3 position = Vector3(0,0,0);
	Vector3 rotationAxis = Vector3(0,1,0);
	float rotationAngle = 0;;
	float mass = 0;
	Vector3 scale = Vector3(1,1,1);
	bool gravity = false;
};

class XLRemovePhysicsEntityEvent : public XLPhysicsEvent {
public:
	XLRemovePhysicsEntityEvent() { type = REMOVE_PHYSICS_ENTITY; }
	~XLRemovePhysicsEntityEvent() {}

	string id;
};

class XLPositionEvent : public XLPhysicsEvent {
public:
	XLPositionEvent() { type = POSITION; };
	~XLPositionEvent() {};

	string id;
	Vector3 position;
	Vector3 rotationAxis;
	float rotationAngle;
};

class XLCollisionEvent : public XLPhysicsEvent {
public:
	XLCollisionEvent() { type = COLLISION; }
	XLCollisionEvent(XLCollisionEvent& e) {
		type = COLLISION;
		idA = e.idA;
		idB = e.idB;
		collisionPointsOnA = e.collisionPointsOnA;
		collisionPointsOnB = e.collisionPointsOnB;
		normalWorldOnB = e.normalWorldOnB;
	}
	~XLCollisionEvent() {}

	string idA;
	string idB;

	vector<Vector3> collisionPointsOnA;
	vector<Vector3> collisionPointsOnB;
	vector<Vector3> normalWorldOnB;
};

class XLForceEvent : public XLPhysicsEvent {
public:
	XLForceEvent() { type = FORCE; }
	~XLForceEvent() {}

	string id;
	Vector3 force = Vector3(0,1,0);
	Vector3 forceLocation = Vector3(0,0,0);
};

class XLImpulseEvent : public XLPhysicsEvent {
public:
	XLImpulseEvent() { type = IMPULSE; }
	~XLImpulseEvent() {}

	string id;
	Vector3 impulse = Vector3(0,1,0);
	Vector3 impulseLocation = Vector3(0,0,0);
};

class XLMovementLimiterEvent : public XLPhysicsEvent {
public:
	XLMovementLimiterEvent() { type = LIMIT_MOVEMENT; }
	~XLMovementLimiterEvent() {};

	string id;
	Vector3 axis = Vector3(1, 1, 1);
};

class XLRotationLimiterEvent : public XLPhysicsEvent {
public:
	XLRotationLimiterEvent() { type = LIMIT_ROTATION; }
	~XLRotationLimiterEvent() {};

	string id;
	Vector3 axis = Vector3(1, 1, 1);
};

class XLGetVelocityEvent : public XLPhysicsEvent {
public:
	XLGetVelocityEvent() { type = GET_VELOCITY; }
	~XLGetVelocityEvent() {}

	string id;
};

class XLReturnVelocityEvent : public XLPhysicsEvent {
public:
	XLReturnVelocityEvent() { type = RETURN_VELOCITY; }
	XLReturnVelocityEvent(XLReturnVelocityEvent& e) {
		type = RETURN_VELOCITY;
		id = e.id;
		velocity = e.velocity;
	}
	~XLReturnVelocityEvent() {}

	string id;
	Vector3 velocity;
};

class XLTorqueEvent : public XLPhysicsEvent {
public:
	XLTorqueEvent() { type = TORQUE; }
	~XLTorqueEvent() {}

	string id;
	Vector3 torque = Vector3(1,1,1);
};

class XLAngularForceEvent : public XLPhysicsEvent {
public:
	XLAngularForceEvent() { type = ANGULAR_FORCE; }
	~XLAngularForceEvent() {}

	string id;
	Vector3 force = Vector3(1,1,1);
};

class XLRotateEvent : public XLPhysicsEvent {
public:
	XLRotateEvent() { type = TRANSFORM_ROTATE; }
	~XLRotateEvent() {}

	string id;
	float yaw = 0;
	float pitch = 0;
	float roll = 0;
};

class XLTranlateEvent : public XLPhysicsEvent {
public:
	XLTranlateEvent() { type = TRANSFORM_TRANSLATE; }
	~XLTranlateEvent() {}

	string id;
	Vector3 translation = Vector3(0, 0, 0);
};

class XLSetPositionEvent : public XLPhysicsEvent {
public:
	XLSetPositionEvent() { type = SET_POSITION; }
	~XLSetPositionEvent() {}

	string id;
	Vector3 position = Vector3(0, 0, 0);
};

class XLSetRotationEvent : public XLPhysicsEvent {
public:
	XLSetRotationEvent() { type = SET_ROTATION; }
	~XLSetRotationEvent() {};

	string id;
	float angle = 0;
	Vector3 axis = Vector3(0, 1, 0);
};

class XLCastRayEvent : public XLPhysicsEvent {
public:
	XLCastRayEvent() { type = CAST_RAY; }
	~XLCastRayEvent() {};

	string rayId = "";
	Vector3 start = Vector3(0,0,0);
	Vector3 end = Vector3(1,1,1);
};

class XLReturnRayEvent : public XLPhysicsEvent {
public:
	XLReturnRayEvent() { type = RETURN_RAY_INFO; }
	XLReturnRayEvent(XLReturnRayEvent& e) {
		rayId = e.rayId;
		start = e.start;
		end = e.end;
		hasHit = e.hasHit;
		hitPoint = e.hitPoint;
		hitEntityId = e.hitEntityId;
	}
	~XLReturnRayEvent() {}

	string rayId = "";
	Vector3 start;
	Vector3 end;

	bool hasHit = false;
	Vector3 hitPoint;
	string hitEntityId = "";
};

class XLCastRayAllHitsEvent : public XLPhysicsEvent {
public:
	XLCastRayAllHitsEvent() { type = CAST_RAY_ALL_HITS; }
	~XLCastRayAllHitsEvent() {};

	string rayId = "";
	Vector3 start = Vector3(0, 0, 0);
	Vector3 end = Vector3(1, 1, 1);
};

class XLReturnRayAllHitsEvent : public XLPhysicsEvent {
public:
	XLReturnRayAllHitsEvent() { type = RETURN_RAY_ALL_HITS_INFO; }
	XLReturnRayAllHitsEvent(XLReturnRayAllHitsEvent& e) {
		type = RETURN_RAY_ALL_HITS_INFO;
		rayId = e.rayId;
		start = e.start;
		end = e.end;
		hasHit = e.hasHit;
		hitPoints = e.hitPoints;
		hitEntityIds = e.hitEntityIds;
	}
	~XLReturnRayAllHitsEvent() {}

	string rayId = "";
	Vector3 start;
	Vector3 end;

	bool hasHit = false;
	vector<Vector3> hitPoints;
	vector<string> hitEntityIds;
};