#include "stdafx.h"
#include "PhysicsEntity.h"


PhysicsEntity::PhysicsEntity(string parentId, cShape shape, const btVector3& position, const btQuaternion& rotation, const btVector3& scale, float mass, bool gravity)
{
	this->parentId = parentId;

	setMass(mass);
	setGravity(gravity);

	switch(shape) {
	case(PHYSICS_BOUNDING_PLANE): {
		collisionShape = new btStaticPlaneShape(scale, 1);
		break;
	}
	case(PHYSICS_SPHERE): {
		collisionShape = new btSphereShape(scale.getX());
		break;
	}
	case (PHYSICS_BOX): {
		collisionShape = new btBoxShape(scale);
		break;
	}
	case(PHYSICS_CYLINDER): {
		collisionShape = new btCylinderShape(scale);
		break;
	}
	case(PHYSICS_CAPSULE): {
		collisionShape = new btCapsuleShape(scale.getX(), scale.getY());
		break;
	}
	case(PHYSICS_CONE): {
		collisionShape = new btConeShape(scale.getX(), scale.getY());
		break;
	}
	}

	motionState = new btDefaultMotionState(btTransform(rotation, position));
	btVector3 fallInertia(0, 0, 0);
	collisionShape->calculateLocalInertia(mass, fallInertia);
	btRigidBody::btRigidBodyConstructionInfo rigidBodyCI(mass, motionState, collisionShape, fallInertia);
	rigidBody = new btRigidBody(rigidBodyCI);
}


PhysicsEntity::~PhysicsEntity()
{
	delete collisionShape;
	delete motionState;
	delete rigidBody;
}

btRigidBody* PhysicsEntity::getRigidBody() {
	return rigidBody;
}

void PhysicsEntity::reposition(const btVector3& position, const btQuaternion& orientation) {
	btTransform initialTransform;

	initialTransform.setOrigin(position);
	initialTransform.setRotation(orientation);

	rigidBody->setWorldTransform(initialTransform);
	motionState->setWorldTransform(initialTransform);
}

btVector3 PhysicsEntity::getPosition() {
	return rigidBody->getWorldTransform().getOrigin();
}

btQuaternion PhysicsEntity::getRotation() {
	return rigidBody->getWorldTransform().getRotation();
}