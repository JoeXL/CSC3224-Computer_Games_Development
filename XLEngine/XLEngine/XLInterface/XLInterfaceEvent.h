#pragma once
#include "../XLEventQueue/XLEventQueue.h"
#include "../../nclgl/Vector2.h"

enum InterfaceEventType {
	BUTTON_PRESS, MOUSE_DATA, GAMEPAD_DATA, KEYBIND, GAMEPAD_VIBRATE
};

enum InterfaceControllers {
	KEYBOARD, MOUSE, GAMEPAD
};

class XLInterfaceEvent : public XLEvent
{
public:
	XLInterfaceEvent() {};
	~XLInterfaceEvent() {};

	InterfaceEventType getType() { return type; }

protected:
	InterfaceEventType type;
};

class XLInterfaceButtonEvent : public XLInterfaceEvent {
public:
	XLInterfaceButtonEvent(int btn, int func, int control, bool hld) {
		type = BUTTON_PRESS;
		controller = control;
		button = btn;
		function = func;
		held = hld;
	}
	~XLInterfaceButtonEvent() {}

	int button = 0;
	int function = 0;
	int controller = 0;
	bool held = false;
};

class XLMouseDataEvent : public XLInterfaceEvent {
public:
	XLMouseDataEvent(Vector2 relPos, Vector2 absPos, bool wMoved, int wMovement) {
		type = MOUSE_DATA;
		relativePosition = relPos;
		absolutePosition = absPos;
		wheelMoved = wMoved;
		wheelMovement = wMovement;
	}
	~XLMouseDataEvent() {};

	Vector2 relativePosition;
	Vector2 absolutePosition;
	bool wheelMoved;
	int wheelMovement;
};

class XLGamepadDataEvent : public XLInterfaceEvent {
public:
	XLGamepadDataEvent(float thumbLX, float thumbLY, float thumbRX, float thumbRY, float triggerL, float triggerR) {
		type = GAMEPAD_DATA;
		leftThumbX = thumbLX;
		leftThumbY = thumbLY;
		rightThumbX = thumbRX;
		rightThumbY = thumbRY;
		leftTrigger = triggerL;
		rightTrigger = triggerR;
	}
	~XLGamepadDataEvent() {}

	float leftThumbX;
	float leftThumbY;
	float rightThumbX;
	float rightThumbY;
	float leftTrigger;
	float rightTrigger;
};

class XLKeybindEvent : public XLInterfaceEvent {
public:
	XLKeybindEvent(int ctrl, int k, int b) {
		type = KEYBIND;
		controller = ctrl;
		key = k;
		bind = b;
	}
	~XLKeybindEvent() {}

	int controller;
	int key;
	int bind;
};

class XLGamepadVibrateEvent : public XLInterfaceEvent {
public:
	XLGamepadVibrateEvent(float l, float r) {
		left = l;
		right = r;
	}
	~XLGamepadVibrateEvent() {}

	float left;
	float right;
};