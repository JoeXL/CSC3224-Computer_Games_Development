#include "stdafx.h"
#include "XboxController.h"

#pragma comment(lib, "XInput.lib")


XboxController::XboxController(int playerNumber)
{
	controllerNum = playerNumber - 1;

	buttonMap.insert(make_pair(XBOX_DPAD_UP, XINPUT_GAMEPAD_DPAD_UP));
	buttonMap.insert(make_pair(XBOX_DPAD_DOWN, XINPUT_GAMEPAD_DPAD_DOWN));
	buttonMap.insert(make_pair(XBOX_DPAD_LEFT, XINPUT_GAMEPAD_DPAD_LEFT));
	buttonMap.insert(make_pair(XBOX_DPAD_RIGHT, XINPUT_GAMEPAD_DPAD_RIGHT));
	buttonMap.insert(make_pair(XBOX_START, XINPUT_GAMEPAD_START));
	buttonMap.insert(make_pair(XBOX_BACK, XINPUT_GAMEPAD_BACK));
	buttonMap.insert(make_pair(XBOX_LEFT_THUMB, XINPUT_GAMEPAD_LEFT_THUMB));
	buttonMap.insert(make_pair(XBOX_RIGHT_THUMB, XINPUT_GAMEPAD_RIGHT_THUMB));
	buttonMap.insert(make_pair(XBOX_LEFT_SHOULDER, XINPUT_GAMEPAD_LEFT_SHOULDER));
	buttonMap.insert(make_pair(XBOX_RIGHT_SHOULDER, XINPUT_GAMEPAD_RIGHT_SHOULDER));
	buttonMap.insert(make_pair(XBOX_A, XINPUT_GAMEPAD_A));
	buttonMap.insert(make_pair(XBOX_B, XINPUT_GAMEPAD_B));
	buttonMap.insert(make_pair(XBOX_X, XINPUT_GAMEPAD_X));
	buttonMap.insert(make_pair(XBOX_Y, XINPUT_GAMEPAD_Y));
}


XboxController::~XboxController()
{
}

XINPUT_STATE XboxController::getState() {
	ZeroMemory(&controllerState, sizeof(XINPUT_STATE));
	XInputGetState(controllerNum, &controllerState);
	return controllerState;
}

bool XboxController::isConnected() {
	controllerNum = -1;
	ZeroMemory(&controllerState, sizeof(XINPUT_STATE));
	DWORD Result = XInputGetState(controllerNum, &controllerState);
	if (Result == ERROR_SUCCESS) {
		return true;
	}
	else {
		return false;
	}
}

void XboxController::Vibrate(float leftVal, float rightVal) {
	XINPUT_VIBRATION Vibration;
	ZeroMemory(&Vibration, sizeof(XINPUT_VIBRATION));

	if (leftVal < 0) leftVal = 0;
	if (leftVal > 1) leftVal = 1;
	if (rightVal < 0) rightVal = 0;
	if (rightVal > 1) rightVal = 1;

	Vibration.wLeftMotorSpeed = leftVal * 65535;
	Vibration.wRightMotorSpeed = rightVal * 65535;

	XInputSetState(controllerNum, &Vibration);
}

bool XboxController::Refresh() {
	if (controllerNum == -1) {
		isConnected();
	}
	if (controllerNum != -1) {
		ZeroMemory(&controllerState, sizeof(XINPUT_STATE));
		if (XInputGetState(controllerNum, &controllerState) != ERROR_SUCCESS) {
			controllerNum == -1;
			return false;
		}

		setThumbstickValues();

		setTriggerValues();

		return true;
	}
	return false;
}

bool XboxController::isPressed(XboxButtons button) {
	return (controllerState.Gamepad.wButtons & buttonMap[button]) != 0;
}

void XboxController::setThumbstickValues() {
	float normLX = max(-1, (float)getState().Gamepad.sThumbLX / 32767);
	float normLY = max(-1, (float)getState().Gamepad.sThumbLY / 32767);

	if (normLX != 0) {
		leftThumbX = (fabsf(normLX) < stickDeadzoneX ? 0 : (fabsf(normLX) - stickDeadzoneX) * (normLX / fabsf(normLX)));
		if (stickDeadzoneX > 0) leftThumbX *= 1 / (1 - stickDeadzoneX);
	}
	else {
		leftThumbX = 0;
	}
	if (normLY != 0) {
		leftThumbY = (fabsf(normLY) < stickDeadzoneY ? 0 : (fabsf(normLY) - stickDeadzoneY) * (normLY / fabsf(normLY)));
		if (stickDeadzoneY > 0) leftThumbY *= 1 / (1 - stickDeadzoneY);
	}
	else {
		leftThumbY = 0;
	}

	float normRX = max(-1, (float)getState().Gamepad.sThumbRX / 32767);
	float normRY = max(-1, (float)getState().Gamepad.sThumbRY / 32767);

	if (normRX != 0) {
		rightThumbX = (fabsf(normRX) < stickDeadzoneX ? 0 : (fabsf(normRX) - stickDeadzoneX) * (normRX / fabsf(normRX)));
		if (stickDeadzoneX > 0) rightThumbX *= 1 / (1 - stickDeadzoneX);
	}
	else {
		rightThumbX = 0;
	}

	if (normRY != 0) {
		rightThumbY = (fabsf(normRY) < stickDeadzoneY ? 0 : (fabsf(normRY) - stickDeadzoneY) * (normRY / fabsf(normRY)));
		if (stickDeadzoneY > 0) rightThumbY *= 1 / (1 - stickDeadzoneY);
	}
	else {
		rightThumbY = 0;
	}
}

void XboxController::setTriggerValues() {
	float normL = (float)getState().Gamepad.bLeftTrigger / 255;
	float normR = (float)getState().Gamepad.bRightTrigger / 255;

	if (normL != 0) {
		leftTrigger = (fabsf(normL) < triggerDeadzone ? 0 : (fabsf(normL) - triggerDeadzone) * (normL / fabsf(normL)));
	}
	else {
		leftTrigger = 0;
	}

	if (normR != 0) {
		rightTrigger = (fabsf(normR) < triggerDeadzone ? 0 : (fabsf(normR) - triggerDeadzone) * (normR / fabsf(normR)));
	}
	else {
		rightTrigger = 0;
	}

	if (triggerDeadzone > 0) {
		leftTrigger *= 1 / (1 - leftTrigger);
		rightTrigger *= 1 / (1 - rightTrigger);
	}

	std::cout << leftTrigger << std::endl;
}