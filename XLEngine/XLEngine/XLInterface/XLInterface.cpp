#include "stdafx.h"
#include "XLInterface.h"


XLInterface::XLInterface()
{
	xboxController = new XboxController(1);
	eventQueue = new XLEventQueue();
}

XLInterface::~XLInterface()
{
	delete xboxController;
	delete eventQueue;
}

void XLInterface::initialize(Keyboard* kb, Mouse* ms) {
	keyboard = kb;
	mouse = ms;
}

void XLInterface::update(float msec, XLEventQueue* events) {

	processEvents(events);

	for (int i = 0; i < KEYBOARD_MAX; ++i) {
		processKeyboard((KeyboardKeys)i);
	}
	for (int i = 0; i < MOUSE_MAX; ++i) {
		processMouse((MouseButtons)i);
	}
	for (int i = 1; i < XBOX_MAX; ++i) {
		processXbox((XboxButtons)i);
	}
	xboxController->Refresh();

	XLMouseDataEvent* mouseData = new XLMouseDataEvent(mouse->GetRelativePosition(), mouse->GetAbsolutePosition(), mouse->WheelMoved(), mouse->GetWheelMovement());
	XLGamepadDataEvent* gamepadData = new XLGamepadDataEvent(xboxController->leftThumbX, xboxController->leftThumbY,
		xboxController->rightThumbX, xboxController->rightThumbY, xboxController->leftTrigger, xboxController->rightTrigger);

	eventQueue->push(mouseData);
	eventQueue->push(gamepadData);

}

void XLInterface::processKeyboard(KeyboardKeys key) {
	if (keyboardKeybinds.find(key) != keyboardKeybinds.end()) { // If has been bound, if it has been pressed, generate an event
		if (keyboard->KeyDown(key)) {
			bool held = false;
			if (keyboard->KeyHeld(key)) held = true;
			XLInterfaceButtonEvent*e = new XLInterfaceButtonEvent((int)key, keyboardKeybinds[key], KEYBOARD, held);
			eventQueue->push(e);
		}
	}
}

void XLInterface::processMouse(MouseButtons button) {
	if (mouseKeybinds.find(button) != mouseKeybinds.end()) { // If has been bound, if it has been pressed, generate an event
		if (mouse->ButtonDown(button)) {
			bool held = false;
			bool doubleClicked = false;
			if (mouse->ButtonHeld(button)) held = true;
			if (mouse->DoubleClicked(button)) doubleClicked = true;
			XLInterfaceButtonEvent* e = new XLInterfaceButtonEvent(button, mouseKeybinds[button], MOUSE, held);
			eventQueue->push(e);
		}
	}
}

void XLInterface::processXbox(XboxButtons button) {
	if (xboxKeybinds.find(button) != xboxKeybinds.end()) { // If has been bound, if it has been pressed, generate an event
		if (xboxController->isPressed(button)) {
			XLInterfaceButtonEvent* e = new XLInterfaceButtonEvent(button, xboxKeybinds[button], GAMEPAD, false);
			eventQueue->push(e);
		}
	}
}

void XLInterface::processEvents(XLEventQueue* events) {
	while (!events->empty()) {
		switch (((XLInterfaceEvent*)events->front())->getType()) {
		case BUTTON_PRESS: {
			switch (((XLInterfaceButtonEvent*)events->front())->controller) {
			case KEYBOARD: {
				XLInterfaceButtonEvent* e = (XLInterfaceButtonEvent*)events->front();
				delete e;
				break;
			}
			case MOUSE: {
				XLInterfaceButtonEvent* e = (XLInterfaceButtonEvent*)events->front();
				delete e;
				break;
			}
			case GAMEPAD: {
				XLInterfaceButtonEvent* e = (XLInterfaceButtonEvent*)events->front();
				delete e;
				break;
			}
			}
			break;
		}
		case MOUSE_DATA: {
			XLMouseDataEvent* e = (XLMouseDataEvent*)events->front();
			delete e;
			break;
		}
		case GAMEPAD_DATA: {
			XLGamepadDataEvent* e = (XLGamepadDataEvent*)events->front();
			delete e;
			break;
		}
		case KEYBIND: {
			XLKeybindEvent* e = (XLKeybindEvent*)events->front();
			switch (e->controller) {
			case KEYBOARD: {
				bindKeyboardKeybind(e->key, e->bind);
				delete e;
				break;
			}
			case MOUSE: {
				bindMouseButton(e->key, e->bind);
				delete e;
				break;
			}
			case GAMEPAD: {
				bindXboxButton(e->key, e->bind);
				delete e;
				break;
			}
			}
			break;
		}
		case GAMEPAD_VIBRATE: {
			XLGamepadVibrateEvent* e = (XLGamepadVibrateEvent*)events->front();
			xboxController->Vibrate(e->left, e->right);
			delete e;
			break;
		}
		}
		events->popEvent();
	}
}