#pragma once
#include <Windows.h>
#include <Xinput.h>
#include <math.h>
#include <iostream>
#include <map>

/* These are mapped to XINPUT defines in the constructor */
enum XboxButtons {
	XBOX_DPAD_UP,
	XBOX_DPAD_DOWN,
	XBOX_DPAD_LEFT,
	XBOX_DPAD_RIGHT,
	XBOX_START,
	XBOX_BACK,
	XBOX_LEFT_THUMB,
	XBOX_RIGHT_THUMB,
	XBOX_LEFT_SHOULDER,
	XBOX_RIGHT_SHOULDER,
	XBOX_A,
	XBOX_B,
	XBOX_X,
	XBOX_Y,
	XBOX_MAX
};

using namespace std;

class XboxController
{
public:
	XboxController(int playerNumber);
	~XboxController();

	XINPUT_STATE getState();
	bool isConnected();

	/* Takes a percentage between 0 - 1 */
	void Vibrate(float leftVal = 0, float rightVal = 0);
	
	const float& getTriggerDeadzone() { return triggerDeadzone; }
	/* Takes a percentage between 0 - 1 */
	void setTriggerDeadzone(float deadzone) { triggerDeadzone = deadzone; }

	const float& getStickDeadzoneX() { return stickDeadzoneX; }
	const float& getStickDeadzoneY() { return stickDeadzoneY; }

	/* Takes a percentage between 0 - 1 */
	void setStickDeadzoneX(float deadzone) { stickDeadzoneX = deadzone; }
	void setStickDeadzoneY(float deadzone) { stickDeadzoneY = deadzone; }

	/* Sets the controller state and thumb/trigger values. Should be called each frame */
	bool Refresh();
	bool isPressed(XboxButtons button);

	/* These values are all percentages between 0 - 1, scaled depending on the deadzone values */
	float leftThumbX;
	float leftThumbY;
	float rightThumbX;
	float rightThumbY;
	float leftTrigger;
	float rightTrigger;

protected:
	/* Set the current thumb and trigger values */
	void setThumbstickValues();
	void setTriggerValues();

private:
	XINPUT_STATE controllerState;
	int controllerNum;

	float triggerDeadzone = 0;
	float stickDeadzoneX = 0;
	float stickDeadzoneY = 0;

	map<XboxButtons, WORD> buttonMap;
};