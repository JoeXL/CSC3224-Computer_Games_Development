#pragma once
#include "../../nclgl/Keyboard.h"
#include "../../nclgl/Mouse.h"
#include "XboxController.h"
#include "XLInterfaceEvent.h"
#include <map>

class XLInterface
{
public:

	XLInterface();
	~XLInterface();
	void initialize(Keyboard* kb, Mouse* ms);

	void update(float msec, XLEventQueue* events);

	XLEventQueue* getGeneratedEvents() { return eventQueue; }

protected:
	void processKeyboard(KeyboardKeys key);
	void processMouse(MouseButtons button);
	void processXbox(XboxButtons button);

	void bindKeyboardKeybind(int key, int bind) {
		if (keyboardKeybinds.find(key) != keyboardKeybinds.end())
			keyboardKeybinds.erase(key);
		keyboardKeybinds.insert(std::make_pair(key, bind));
	};

	void bindMouseButton(int button, int bind) {
		if (mouseKeybinds.find(button) != mouseKeybinds.end())
			mouseKeybinds.erase(button);
		mouseKeybinds.insert(std::make_pair(button, bind));
	}

	void bindXboxButton(int button, int bind) {
		if (xboxKeybinds.find(button) != xboxKeybinds.end())
			xboxKeybinds.erase(button);
		xboxKeybinds.insert(std::make_pair(button, bind));
	}

	void processEvents(XLEventQueue* events);

private:
	XboxController* xboxController;
	XLEventQueue* eventQueue;
	Keyboard* keyboard;
	Mouse* mouse;

	std::map<int, int> keyboardKeybinds;
	std::map<int, int> mouseKeybinds;
	std::map<int, int> xboxKeybinds;
};

