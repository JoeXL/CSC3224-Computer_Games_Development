# Game Development Coursework
## Game Mechanics
### Objective
Kill all the enemies in an area to unlock the exit

Enemies move towards you when they have line of sight

You take damage when an enemy touces you

### Video Demo

https://www.youtube.com/watch?v=6xcIqSaY86U&feature=youtu.be

### Controls
#### Keyboard & Mouse
* WSAD - Move the player
* Mouse - Rotate the player
* Left Click - Shoot

#### Controller
* Left Stick - Move the player
* Right Stick - Rotate the player
* R1 - Shoot

## Build Information
The solution file is in the **XLEngine/XLEngine** folder

It is currently only configured for Win32

Startup project should be set to **Game**

Build directories are in **XLEngine/XLEngine** then either **Release** or **Debug**. Settings/Startup files can be found in the respective build folders
